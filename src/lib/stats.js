import { getModelDetail } from './models.js'

export const getPrice = (tokens, model) => {
  const t = getModelDetail(model)
  return ((tokens.prompt_tokens * (t.prompt || 0)) + (tokens.completion_tokens * (t.completion || 0)))
}

export const countPromptTokens = (prompts, model, chat) => {
  return getModelDetail(model).countPromptTokens(prompts, model, chat)
}

export const countMessageTokens = (message, model, chat) => {
  return getModelDetail(model).countMessageTokens(message, model, chat)
}

export const getModelMaxTokens = (model) => {
  return getModelDetail(model).max
}

