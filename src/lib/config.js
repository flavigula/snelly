let host = "localhost";
let password = "thurk";
if(import.meta.env.VITE_SES_TENXIL) {
  password = "";
}

const isLocal = import.meta.env.VITE_LOCAL || import.meta.env.VITE_SES_TENXIL || import.meta.env.VITE_TAHR;

const postgresOpts = {
  host,
  user: 'vdna',
  password,
  database: "snelly",
};

let zepApiURL = "http://zep.thurk.org";
if(import.meta.env.VITE_TAHR) {
  // Yak address
  zepApiURL = "http://10.47.0.2:8000";
} else if(import.meta.env.VITE_SES_TENXIL || import.meta.env.VITE_LOCAL) {
  zepApiURL = "http://localhost:8000";
} else if(import.meta.env.VITA_ZEP_THURK) {
  zepApiURL = "http://zep.thurk.org";
}
// Default nuevo-thurk (staging)
let zepApiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.hdSLW1Ph_e6e3f1YQc-2vqJFVI1uuzmVOr3IBYWPMVc";
if(import.meta.env.VITE_TAHR) {
  // This is the jwt_token for YAK
  zepApiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.XAdi1fcFeEkZSa6ahUyvHHe-nkYQunhNvKbxe1wNN_o";
} else if(import.meta.env.VITE_SES_TENXIL) {
  zepApiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.5BrRsMk8AClfbqGYGtW9cTQx2m3UeD9wcRrjHXxbB4o";
}

export default { postgresOpts, zepApiURL, zepApiKey, isLocal };
