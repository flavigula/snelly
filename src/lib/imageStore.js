import Dexie from 'dexie'
import { v4 as uuidv4 } from 'uuid'

let _hasIndexedDb = false; // !!window.indexedDB
const dbCheck = _hasIndexedDb; // && window.indexedDB.open('test');
if (_hasIndexedDb) dbCheck.onerror = () => { _hasIndexedDb = false };

let imageCache = {};

class ChatImageStore extends Dexie {
  constructor () {
    super('chatImageStore')
    this.version(1).stores({
      images: 'id' // Primary key and indexed props
    })
  }
}

const imageDb = new ChatImageStore();

export const hasIndexedDb = () => _hasIndexedDb;

export const getImage = async (uuid) => {
  let image = imageCache[uuid]
  if (image || !_hasIndexedDb) return image
  image = await imageDb.images.get(uuid);
  imageCache[uuid] = image
  return image
}

export const deleteImage = async (chatId, uuid) => {
  const cached = imageCache[uuid];
  if (cached) cached.chats = cached.chats?.filter(c => c !== chatId);
  if (!cached?.chats?.length) delete imageCache[uuid];
  if (_hasIndexedDb) {
    const stored = await imageDb.images.get({ id: uuid });;
    if (stored) stored.chats = stored.chats?.filter(c => c !== chatId)
    if (!stored?.chats?.length) {
      imageDb.images.delete(uuid);
    } else if (stored) {
      await setImage(chatId, stored);
    }
  }
}

  export const clearAllImages = async () => {
    imageCache = {}
    if (_hasIndexedDb) {
      imageDb.images.clear()
    }
  }

export const setImage = async (chatId, image) => {
  image.id = image.id || uuidv4()
  let current;
  if (_hasIndexedDb) {
    current = await imageDb.images.get({ id: image.id });
  } else {
    current = imageCache[image.id]
  }
  current = current || image
  current.chats = current.chats || []
  if (!(chatId in current.chats)) current.chats.push(chatId)
  imageCache[current.id] = current
  if (_hasIndexedDb) {
    imageDb.images.put(current, current.id)
  }
  const clone = JSON.parse(JSON.stringify(current))
  // Return a copy without the payload so local storage doesn't get clobbered
  delete clone.b64image
  delete clone.chats
  return clone
}

