/* From Server Side */

export const toZep = async (fetch, username, messages, chatId, stream) => {
  const reader = stream.getReader();
  let chatResponse = '';
  while(true) {
    const res = await reader.read();
    if(res.done) break;
    if(res.value) {
      const intObj = Object.keys(res.value).reduce((intObj, keyStr) => {
        intObj[parseInt(keyStr)] = res.value[keyStr];
        return intObj;
      }, {});
      const message = Object.keys(intObj).sort((a, b) => a - b).reduce((message, key) => {
        return `${message}${String.fromCharCode(intObj[key])}`;
      }, "");
      chatResponse = `${chatResponse}${message}`;
    }
  }
  console.log(`CHAT RESPONSE: ${chatResponse}`);
  const zepFormData = new FormData();
  zepFormData.set('username', username);
  messages.push({ role: 'ai', content: chatResponse });
  zepFormData.set('messages', JSON.stringify(messages));
  zepFormData.set('chatId', chatId);
  fetch('/api/toZep', {
    method: 'POST',
    body: zepFormData
  });
  return chatResponse;
};

