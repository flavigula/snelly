// This makes it possible to override the OpenAI API base URL in the .env file
const apiBase = import.meta.env.VITE_API_BASE || 'https://api.openai.com';
const endpointCompletions = import.meta.env.VITE_ENDPOINT_COMPLETIONS || '/v1/chat/completions';
const endpointGenerations = import.meta.env.VITE_ENDPOINT_GENERATIONS || '/v1/images/generations';
const endpointModels = import.meta.env.VITE_ENDPOINT_MODELS || '/v1/models';
const endpointEmbeddings = import.meta.env.VITE_ENDPOINT_EMBEDDINGS || '/v1/embeddings';
const petalsBase = import.meta.env.VITE_PEDALS_WEBSOCKET || 'wss://chat.petals.dev';
const endpointPetals = import.meta.env.VITE_PEDALS_WEBSOCKET || '/api/v2/generate';

export const getApiBase = () => apiBase;
export const getEndpointCompletions = () => endpointCompletions;
export const getEndpointGenerations = () => endpointGenerations;
export const getEndpointModels = () => endpointModels;
export const getEndpointEmbeddings = () => endpointEmbeddings;
export const getPetalsBase = () => petalsBase;
export const getPetalsWebsocket = () => endpointPetals;
