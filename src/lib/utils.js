import { compare } from 'stacking-order';
import { openModal } from 'svelte-modals';

/* based on gpt4 */
export const price = (role, tokenCount) => {
  const p = (role === 'human' || role === 'user' ? 0.03 : 0.06) * tokenCount / 1000;
  return `$${p.toFixed(6)}`;
};

export const promptCategoriesFromPromptCategoryPath = (promptCategories, promptCategoryPath) => {
  let path = Object.assign([], promptCategoryPath);
  let catPath = [];
  let categories = promptCategories;
  // console.log(`/(app)/+page.svelte > categories: ${JSON.stringify(categories, null, "  ")}`);
  let hasPromptId = null;
  while(path.length > 0) {
    console.log(`utils.js > promptCategoriesFromPromptCategoryPath > path: ${JSON.stringify(path)}`);
    const id = path.shift();
    const base = categories.find(category => {
      return category[0].id === id;
    });
    if(base && base[0]) {
      catPath.push(base[0]);
      if(base[0].prompt_id) {
        hasPromptId = base[0].prompt_id;
        break;
      }
    }
    // console.log(`/(app)/+page.svelte > id: ${id} base: ${JSON.stringify(base)}`);
    categories = (base && base[1]) || [];
  }
  return { catPath, cats: categories, hasPromptId };
};

const zippy = (categoryChunk, id) => {
  console.log(`zippy > categoryChunk: ${JSON.stringify(categoryChunk)}`);
  if(!categoryChunk || categoryChunk.length === 0) return null;
  const category = categoryChunk[0];
  if(category.id === id) return id;
  return categoryChunk[1].map(chunk => {
    const path = zippy(chunk, id);
    if(!path) return null;
    return [ category.id ].concat([ path ]);
  });
};
export const findPromptPathFromPromptId = (categories, id) => {
  const paths = categories.map(categoryChunk => zippy(categoryChunk, id));
  console.log(`findPromptPathFromPromptId > paths: ${JSON.stringify(paths)}`);
};

export const isAdmin = async ({ id }) => {
  const res = await fetch(`/api/isAdmin/${id}`, { method: 'post' });
  if(res.status !== 200) return false;
  const json = await res.json();
  if(!json.success) return false;
  return true;
};

const grabChildren = (rows, id) => {
  return rows.filter(r => r.child_of_id === id);
};

export const recurse = (rows, row) => {
  const children = grabChildren(rows, row.id);
  // console.log(`/(app)/+layout.server.js > recurse > children are: ${JSON.stringify(children)}`);
  return [
    row,
    children.map(child => recurse(rows, child))
  ];
};

export const hasChildren = (allCategories, { id }) => allCategories.reduce(
  (theTruth, category) => {
    return category.child_of_id === id || theTruth;
  }, false
);

export const sizeTextElements = (force = false) => {
  const els = document.querySelectorAll('textarea.auto-size')
  for (let i = 0, l = els.length; i < l; i++) {
    autoGrowInput(els[i], force)
  }
}

export const autoGrowInputOnEvent = (event) => {
  // Resize the textarea to fit the content - auto is important to reset the height after deleting content
  if (event.target === null) return;
  (event.target).__didAutoGrow = false;
  autoGrowInput(event.target);
}

export const autoGrowInput = (el, force = false) => {
  const anyEl = el;// Oh how I hate typescript.  All the markup of Java with no real payoff..
  if (force || !anyEl.__didAutoGrow) el.style.height = '38px' // don't use "auto" here.  Firefox will over-size.
  el.style.height = el.scrollHeight + 'px'
  setTimeout(() => {
    if (el.scrollHeight > el.getBoundingClientRect().height + 5) {
      el.style.overflowY = 'auto'
    } else {
      el.style.overflowY = ''
    }
  }, 0)
  anyEl.__didAutoGrow = true // don't resize this one again unless it's via an event
}

export const scrollIntoViewWithOffset = (element, offset, instant = false, bottom = false) => {
  const behavior = instant ? 'instant' : 'smooth'
  if (bottom) {
    window.scrollTo({
      behavior: behavior,
      top:
      (element.getBoundingClientRect().bottom) -
        document.body.getBoundingClientRect().top - (window.innerHeight - offset)
    })
  } else {
    window.scrollTo({
      behavior: behavior,
      top:
      element.getBoundingClientRect().top -
        document.body.getBoundingClientRect().top -
        offset
    })
  }
}

export const scrollToMessage = (uuid, offset = 60, instant = false, bottom = false) => {
  if (Array.isArray(uuid)) {
    uuid = uuid[0]
  }
  if (!uuid) {
    console.error('Not a valid uuid', uuid)
    return
  }
  const el = document.getElementById('message-' + uuid)
  if (el) {
    scrollIntoViewWithOffset(el, offset, instant, bottom)
  } else {
    console.error("Can't find element with message ID", uuid)
  }
}

export const scrollToBottom = (instant = false) => {
  const body = document.querySelector('body');
  setTimeout(() => {
    if(body) {
      body.scrollIntoView({ behavior: (instant ? 'instant' : 'smooth'), block: 'end' });
    }
  }, 0);
}


export const checkModalEsc = (event) => {
  if (!event || event.key !== 'Escape') return
    dispatchModalEsc()
}

export const dispatchModalEsc = () => {
  const stack = Array.from(document.querySelectorAll('.modal, .has-esc')).filter(s =>
      window.getComputedStyle(s).getPropertyValue('display') !== 'none'
    )
    const top = stack.length === 1
    ? stack[0]
    : stack.find(m1 => {
        return stack.find(m2 => {
          return m1 !== m2 && compare(m1, m2) > 0 && m1
        })
      });
    if (top) {
      // trigger modal-esc event on topmost modal when esc key is pressed
      const e = new CustomEvent('modal-esc', { detail: top })
    top.dispatchEvent(e)
}
}

  export const encodeHTMLEntities = (rawStr) => {
    // return rawStr.replace(/[\u00A0-\u9999<>&]/g, (i) => `&#${i.charCodeAt(0)};`)
  }

export const errorNotice = (message, error = undefined) => {
  /*
  openModal(PromptNotice, {
    title: 'Error',
    class: 'is-danger',
    message: message + (error ? '<br>' + error.message : ''),
    asHtml: true,
    onConfirm: () => {}
  })
  */
}

export const warningNotice = (message, error = undefined) => {
  /*
  openModal(PromptNotice, {
    title: 'Warning',
    class: 'is-warning',
    message: message + (error ? '<br>' + error.message : ''),
    asHtml: true,
    onConfirm: () => {}
  })
  */
}

export const startNewChatFromChatId = (chatId) => {
  // const newChatId = addChat(getChat(chatId).settings)
  // go to new chat
  // replace(`/chat/${newChatId}`)
}

export const startNewChatWithWarning = (activeChatId, profile) => {
  const newChat = () => {
    const chatId = addChat(profile)
    // replace(`/chat/${chatId}`)
  }
  // if (activeChatId && getChat(activeChatId).settings.isDirty) {
  //   openModal(PromptConfirm, {
  //     title: 'Unsaved Profile',
  //     message: '<p>There are unsaved changes to your current profile that will be lost.</p><p>Discard these changes and continue with new chat?</p>',
  //     asHtml: true,
  //     class: 'is-warning',
  //     onConfirm: () => {
  //       newChat()
  //     }
  //   })
  // } else {
  //   newChat()
  // }
  newChat()
}

export const valueOf = (chatId, value) => {
  if (typeof value === 'function') return value(chatId)
  return value
}

export const escapeRegex = (string) => {
  // return string.replace(/[/\-\\^$*+?.()|[\]{}]/g, '\\$&')
}

export const sortCatsByFavourite = (cats, categoryFavourites) => {
  // console.log(`sortCatsByFavourite.js > cats: ${JSON.stringify(cats)}`);
  // console.log(`sortCatsByFavourite.js > categoryFavourites: ${JSON.stringify(categoryFavourites)}`);
  return cats.sort((a, b) => {
    const aIsFavourite = categoryFavourites.includes(a.id || a[0].id);
    const bIsFavourite = categoryFavourites.includes(b.id || b[0].id);
    if(aIsFavourite && !bIsFavourite) return -1;
    if(!aIsFavourite && bIsFavourite) return 1;
    return 0;
  });
};

export const sortCatsByHasPrompt = (cats) => {
  return cats.sort((a, b) => {
    const aHasPrompt = !!a.prompt_id;
    const bHasPrompt = !!b.prompt_id;
    if(aHasPrompt && !bHasPrompt) return -1;
    if(!aHasPrompt && bHasPrompt) return 1;
    return 0;
  });
};

export const removeBrsFromStart = message => {
  const re = /^<br.*?>/;
  if(re.exec(message)) {
    const newMessage = message.replace(re, "");
    return removeBrsFromStart(newMessage);
  } else {
    return message;
  }
};
