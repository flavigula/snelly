/* From Client Side */

export const zepMemory = async (chatId) => {
  const res = await fetch(`/api/zepMemory/${chatId}`, { method: 'POST' });
  if(res && res.status === 200) {
    const json = await res.json();
    if(json && json.messages) {
      return json.messages;
    } else {
      console.log(`zep.js > zepMemory > could not parse json: ${JSON.stringify(json)}`);
      return [];
    }
  } else {
    console.log(`zep.js > zepMemory > fetch returned ${res.status}`);
    return [];
  }
};
