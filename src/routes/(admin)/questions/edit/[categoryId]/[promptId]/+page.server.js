import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const load = async ({ params }) => {
  const { categoryId, promptId } = params;
  let prompt, questions;
  let client;
  if(promptId) {
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(`SELECT p.id, p.title, p.prompt_category_ids, p.question_ids, p.maximum_iterations, st.template AS system_template, ut.template as user_template, ct.template as conclusion_template FROM prompts p, templates st, templates ut, templates ct WHERE p.id = ${promptId} AND p.system_template_id = st.id AND p.user_template_id = ut.id AND (p.conclusion_template_id = NULL OR p.conclusion_template_id = ct.id);`);
      if(res && res.rows && res.rows.length > 0) {
        prompt = res.rows[0];
        const orPart = prompt.question_ids.map(id => `id = ${id}`).join(" OR ");
        const qRes = await client.query(`SELECT * FROM questions WHERE ${orPart} ORDER BY id;`);
        if(qRes && qRes.rows) {
          questions = qRes.rows;
        }
      }
    } catch(err) {
      console.log(`/(admin)/questions/[categoryId]/[promptId]/+page.js > could not load prompt with id ${promptId}: ${err}`);
    } finally {
      if(client) {
        await client.end();
      }
    }
  }
  // console.log(`/(admin)/questions/new/[categoryId] > categoryId : ${categoryId}`);
  return {
    categoryId: categoryId || null,
    prompt,
    questions: questions || []
  };
};
