export const load = ({ params }) => {
  const { categoryId } = params;
  // console.log(`/(admin)/questions/new/[categoryId] > categoryId : ${categoryId}`);
  return {
    categoryId: categoryId || null
  };
};
