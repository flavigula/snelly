import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const load = async () => {
  let prompts = [];
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(`SELECT p.*, st.template AS system_template, ut.template as user_template from prompts p, templates st, templates ut WHERE p.system_template_id = st.id AND p.user_template_id = ut.id;`);
    if(res && res.rows) {
      return { prompts: res.rows };
    }
  } catch(err) {
    console.log(`/(admin)/prompts/+page.server.js > load > postgres err? ${err}`);
    return { prompts };
  } finally {
    if(client) {
      await client.end();
    }
  }
};
