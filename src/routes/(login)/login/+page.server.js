import ch from 'create-hash';
import pg from 'pg';
import config from '$lib/config.js';
import { v4 as uuidv4 } from 'uuid';
const { Client } = pg;

const hashPassword = (password) => {
  const _hash = ch('sha256');
  _hash.update(password);
  const hashedPassword = _hash.digest('hex');
  return hashedPassword;
};

const makeZepToken = async(userId) => {
  let client, zepToken;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    zepToken = uuidv4();
    await client.query(`UPDATE users SET zep_token = '${zepToken}' WHERE id = ${userId};`);
    return zepToken;
  } catch(err) {
    console.log(`/(login)/login/+page.server.js > makeZepToken > err: ${err}`);
    return zepToken;
  } finally {
    if(client) {
      await client.end();
    }
  }
};

const byUsername = async (username) => {
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(`SELECT * from users where username = '${username}';`);
    if(res && res.rows && res.rows.length > 0) {
      const user = res.rows[0];
      const profileRes = await client.query(`SELECT * FROM user_profiles WHERE user_id = ${user.id}`);
      if(profileRes && profileRes.rows && profileRes.rows.length > 0) {
        user.profile = profileRes.rows[0];
      }
      return user;
    } else {
      return null;
    }
  } catch(err) {
    console.log(`/(login)/login/+page.server.js > usernameExists > err: ${err}`);
    return null;
  } finally {
    if(client) {
      await client.end();
    }
  }
};

const newUser = async (username, email, name, password, instantAdmin = false) => {
  const hashedPassword = hashPassword(password);
  const expiry = new Date().getTime() + (60 * 60 * 5 * 1000); // 5 hours
  const token = hashPassword(new Date().toString());
  const zepToken = uuidv4();
  const verificationToken = uuidv4();
  const vtExpiry = new Date().getTime() + (3600 * 24 * 1000); // One day
  // const vtExpiry = new Date().getTime() + (60 * 2 * 1000); // Two minutes
  const isVerified = instantAdmin;
  const isAdmin = instantAdmin;
  const chatsRemaining = 13;
  const now = new Date();
  const trialExpiry = new Date().getTime() + 3600 * 24 * 30 * 1000; // 30 days
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(
      `INSERT INTO users(username, password, email, name, token, zep_token, expiry, created_at, updated_at, is_free_trial, free_trial_expiry, chats_remaining, verification_token, verification_token_expiry, is_verified) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING *;`,
      [
        username,
        hashedPassword,
        email,
        name,
        token,
        zepToken,
        expiry,
        new Date(),
        new Date(),
        !instantAdmin,
        trialExpiry, 
        chatsRemaining,
        verificationToken,
        vtExpiry,
        isVerified
      ]
    );
    if(res && res.rows && res.rows.length > 0) {
      const user = res.rows[0];
      const profileRes = await client.query(`INSERT INTO user_profiles(user_id, info) VALUES(${user.id}, '') RETURNING *`);
      if(profileRes && profileRes.rows && profileRes.rows.length > 0) {
        user.profile = profileRes.rows[0];
      }
      return user;
    } else {
      return false;
    }
  } catch(err) {
    console.log(`/(login)/login/+page.server.js > newUser > err:
${err}`); return false;
  } finally {
    if(client) {
      await client.end();
    }
  }
};

/** @type {import('./$types').Actions} */
export const actions = {
  login: async ({ request }) => {
    const data = await request.formData();
    console.log(`/(login)/login/+page.server.js > login > username: ${data.get('username')} password: ${data.get('password')}`);
    const username = data.get('username') && data.get('username').trim();
    const password = data.get('password');
    if(username.length > 0 && password.length > 0) {
      const hashedPassword = hashPassword(password);
      let client;
      try {
        client = new Client(config.postgresOpts);
        await client.connect();
        const selectQuery = "SELECT password FROM users WHERE username = $1;"
        const res = await client.query(selectQuery, [username]);
        let status = { };
        if(res && res.rows && res.rows.length > 0) {
          if(res.rows[0] && res.rows[0].password && res.rows[0].password === hashedPassword) {
            const expiry = new Date().getTime() + (60 * 60 * 5 * 1000); // 5 hours
            const token = hashPassword(new Date().toString());
            const updateQuery = 'UPDATE users SET token = $1, expiry = $2 WHERE username = $3 RETURNING *;';
            const res = await client.query(updateQuery, [token, expiry, username]);
            if(res && res.rows && res.rows.length > 0) {
              const user = res.rows[0];
              if(!user.zep_token) {
                user.zep_token = await makeZepToken(user.id);
              }
              console.log(`the zep token is: ${user.zep_token}`);
              status.success = {};
              const profileRes = await client.query(`SELECT * FROM user_profiles WHERE user_id = ${user.id}`);
              if(profileRes && profileRes.rows && profileRes.rows.length > 0) {
                status.success.profile = profileRes.rows[0];
              }
              status.success.signup = false;
              status.success.token = token;
              status.success.id = user.id;
              status.success.username = username;
              status.success.email = user.email;
              status.success.name = user.name;
              status.success.expiry = user.expiry;
              status.success.isAdmin = user.is_admin;
              status.success.zepToken = user.zep_token;
              status.success.isFreeTrial = user.is_free_trial;
              status.success.freeTrialExpiry = user.free_trial_expiry;
              status.success.chatsRemaining = user.chats_remaining;
              status.success.verificationToken = user.verification_token;
              status.success.vtExpiry = user.verification_token_expiry;
              status.success.isVerified = user.is_verified;

            } else {
              status.error = `Could not update database for user ${username}`;
            }
          } else {
            status.error = 'Password incorrect';
          }
        } else {
          status.error = 'Username not found';
        }
        await client.end();
        // console.log(`/(login)/login/+page.server.js > status: ${JSON.stringify(status)}`);
        return status;
      } catch(err) {
        console.log(`/login/+page.server.js > postgres error? ${err}`);
        return { error: err };
      } finally {
        if(client) {
          await client.end();
        }
      }
    } else {
      return { error: 'Username & Password cannot be blank' };
    }
  },
  signup: async ({ request }) => {
    const data = await request.formData();
    console.log(`username: ${data.get('username')} name: ${data.get('name')} password: ${data.get('password')}`);
    const username = data.get('username') && data.get('username').trim();
    const name = data.get('name') && data.get('name').trim();
    const password = data.get('password');
    const passwordConfirmation = data.get('passwordConfirmation');
    if(password !== passwordConfirmation) {
      return { 
        error: "Password and password confirmation do not match",
        signup: true
      };
    }
    const email = data.get('email');
  let re = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
    if(!re.exec(email.trim())) {
      return {
        error: 'That is not an email address',
        signup: true
      };
    }
    let instantAdmin = false;
    if(username.match(/^(bobbus|martin|tony)$/)) {
      instantAdmin = true;
      /*
      return { 
        error: 'We are but three',
        signup: true
      };
      */
    }
    if(username.length > 0 && password.length > 0) {
      const user = await byUsername(username);
      if(!!user) {
        return { 
          error: 'That username is taken',
          signup: true
        };
      } else {
        const res = await newUser(username, email.trim(), name, password, instantAdmin);
        if(!!res) {
          return { 
            success: { 
              signup: true,
              username, name, id: res.id, email: res.email,
              token: res.token, expiry: res.expiry,
              isAdmin: res.is_admin,
              zepToken: res.zep_token,
              isFreeTrial: res.is_free_trial,
              freeTrialExpiry: res.free_trial_expiry,
              chatsRemaining: res.chats_remaining,
              verificationToken: res.verification_token,
              isVerified: res.is_verified
            } 
          };
        } else {
          return { 
            error: 'Could not write to the database',
            signup: true
          };
        }
      }
    } else {
      return { error: 'Username & Password both need to contain at least a few characters' };
    }
  }
};
