// import { render } from 'svelte/server';
// import ConfirmEmail from '$lib/email/ConfirmEmail.svelte';
import nodemailer from 'nodemailer';

export const POST = async ({ request, fetch }) => {
  const data = await request.formData();
  const userStr = data.get('user');
  const local = data.get('local');
  console.log(`/outgoingConfirmationEmail > what type is local? ${typeof local}, and its value? ${local}`);
  let user;
  try {
    user = JSON.parse(userStr);
  } catch {
    // parse failed
  }
  if(user) {
    console.log(`outgoingConfirmationEmail > POST > user: ${JSON.stringify(user)}`);
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: 'snellytenxil@gmail.com',
        pass: 'lidf lexg gitn tann'
      }
    });
    /*
    const emailHtml = render(ConfirmEmail, {
      props: {
        name: user.name,
        local: true,
        token: user.verificationToken
      }
    });
    */
    const url = local ? "http://localhost:5005" : "https://snelly.thurk.org";
    const text = `Hello, ${user.name}!\n\nThank you for signing up for Snelly.\nPlease click the following link to confirm your email address and THUS begin using Snelly as soon as possible: ${url}/confirmEmail/${user.verificationToken}\n\n- The Snelly Team (or a marmot impersonating them)`;
    const options = {
      from: 'snellytenxil@gmail.com',
      to: user.email,
      subject: 'Confirm your email address',
      // html: emailHtml
      text
    };
    transporter.sendMail(options);
    return new Response(JSON.stringify({
      status: 'ok'
    }), 
    {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  } else {
    return new Response(JSON.stringify({
      error: `/api/outgoingConfirmationEmail > POST > bad data? userStr: ${userStr}`
    }), 
    {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
