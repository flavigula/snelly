import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const GET = async ({ params }) => {
  const { token } = params;
  if(token) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(
        `UPDATE users set is_verified = TRUE where verification_token = '${token}' RETURNING id;`
      );
      let status = {};
      if(res && res.rows && res.rows.length > 0) {
        status = { status: 'ok' };
      } else {
        status = { error: `Token ${token} not found` };
      }
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      console.log(`/confirmEmail : ${err}`);
      return new Response(JSON.stringify({ error: err }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: 'No token' }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
