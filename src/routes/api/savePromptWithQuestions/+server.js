import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  const data = await request.formData();
  let client;
  try {
    const id = data.get('id');
    const title = data.get('title');
    const systemTemplate = data.get('systemTemplate');
    const chosenSystemTemplateId = parseInt(data.get('chosenSystemTemplateId'));
    console.log(`/api/savePromptWithQuestions > chosenSystemTemplateId: ${chosenSystemTemplateId}`);
    let newSystemTemplateId;
    const userTemplate = data.get('userTemplate');
    let userTemplateId;
    const conclusionTemplate = data.get('conclusionTemplate') || '';
    let conclusionTemplateId;
    const maximumIterations = data.get('maximumIterations') || 13;
    const _questions = data.get('questions');
    const questions = JSON.parse(_questions);
    const _categoryIds = data.get('categoryIds');
    const categoryIds = JSON.parse(_categoryIds);
    // console.log(`/api/savePromptWithQuestions > has everything come through? ${title} ${JSON.stringify(questions)}`);
    let insertOrUpdateQuery;
    const catIds = categoryIds.join(',');
    const _title = title.replace(/'/, "''");
    const _systemTemplate = systemTemplate.replace(/'/, "''");
    const _userTemplate = userTemplate.replace(/'/, "''");
    const _conclusionTemplate = conclusionTemplate.replace(/'/, "''");
    client = new Client(config.postgresOpts);
    await client.connect();
    if(chosenSystemTemplateId === 0) {
      // We have a new system template, vole! Let's save it!
      // There's a space to save question ids here, but I'm leaving
      // them in the prompts table for now.
      const systemTemplateQuery = `INSERT INTO templates(title, question_ids, template, tipo, created_at, updated_at) VALUES($1, $2, $3, $4, $5, $6) RETURNING id;`;
      const systemTemplateTitle = systemTemplate.split(/\s+/).slice(0, 10).join(" ");
      const systemTemplateValues = [ 
        systemTemplateTitle, 
        '{}', 
        _systemTemplate, 
        'system', 
        new Date(), 
        new Date() 
      ];
      console.log(`/api/savePromptWithQuestions > POST > new system template > systemTemplateQuery: ${systemTemplateQuery}, systemTemplateValues: ${JSON.stringify(systemTemplateValues)}`);
      const systemTemplateRes = await client.query(systemTemplateQuery, systemTemplateValues);
      if(systemTemplateRes && systemTemplateRes.rows && systemTemplateRes.rows.length > 0) {
        newSystemTemplateId = parseInt(systemTemplateRes.rows[0].id);
      }
    }
    const userTemplateQuery = `INSERT INTO templates(title, question_ids, template, tipo, created_at, updated_at) VALUES($1, $2, $3, $4, $5, $6) RETURNING id;`;
    const userTemplateTitle = _userTemplate.split(/\s+/).slice(0, 10).join(" ");
    const userTemplateValues = [ 
      userTemplateTitle, 
      '{}', 
      _userTemplate, 
      'user', 
      new Date(), 
      new Date() 
    ];
    console.log(`/api/savePromptWithQuestions > POST > new user template > userTemplateQuery: ${userTemplateQuery}, userTemplateValues: ${JSON.stringify(userTemplateValues)}`);
    const userTemplateRes = await client.query(userTemplateQuery, userTemplateValues);
    if(userTemplateRes && userTemplateRes.rows && userTemplateRes.rows.length > 0) {
      userTemplateId = parseInt(userTemplateRes.rows[0].id);
    }
    let hasConclusionTemplate = false;
    if(_conclusionTemplate.trim().length > 0) {
      const conclusionTemplateQuery = `INSERT INTO templates(title, question_ids, template, tipo, created_at, updated_at) VALUES($1, $2, $3, $4, $5, $6) RETURNING id;`;
      const conclusionTemplateTitle = _conclusionTemplate.split(/\s+/).slice(0, 10).join(" ");
      const conclusionTemplateValues = [ 
        conclusionTemplateTitle, 
        '{}', 
        _conclusionTemplate,
        'conclusion', 
        new Date(), 
        new Date() 
      ];
      console.log(`/api/savePromptWithQuestions > POST > new conclusion template > conclusionTemplateQuery: ${conclusionTemplateQuery}, conclusionTemplateValues: ${JSON.stringify(conclusionTemplateValues)}`);
      const conclusionTemplateRes = await client.query(conclusionTemplateQuery, conclusionTemplateValues);
      if(conclusionTemplateRes && conclusionTemplateRes.rows && conclusionTemplateRes.rows.length > 0) {
        conclusionTemplateId = parseInt(conclusionTemplateRes.rows[0].id);
      }
      hasConclusionTemplate = true;
    }
    const systemTemplateId = chosenSystemTemplateId === 0 ? newSystemTemplateId : chosenSystemTemplateId;
    if(parseInt(id) === 0) {
      insertOrUpdateQuery = `INSERT INTO prompts (prompt_category_ids, title, system_template_id, user_template_id, maximum_iterations${hasConclusionTemplate ? ', conclusion_template_id' : ''}) VALUES ('{${catIds}}', '${_title}', ${systemTemplateId}, ${userTemplateId}, ${maximumIterations}${hasConclusionTemplate ? ', ' + conclusionTemplateId.toString() : ''}) RETURNING *;`
    } else {
      const conclusionTemplatePart = hasConclusionTemplate ? `, conclusion_template_id = ${conclusionTemplateId}` : '';
      insertOrUpdateQuery = `UPDATE prompts set prompt_category_ids = '{${catIds}}', title = '${_title}', system_template_id = ${systemTemplateId}, user_template_id = ${userTemplateId}, maximum_iterations = ${maximumIterations}${conclusionTemplatePart} WHERE id = ${id} RETURNING *;`;
    }
    console.log(`/api/savePromptWithQuestions > insertOrUpdateQuery is ${insertOrUpdateQuery}`);
    const promptRes = await client.query(insertOrUpdateQuery);
    if(promptRes && promptRes.rows && promptRes.rows.length > 0) {
      const prompt = promptRes.rows[0];
      for(let idx = 0; idx < categoryIds.length; idx++) {
        await client.query(`UPDATE prompt_categories SET prompt_id = ${prompt.id} WHERE id = ${categoryIds[idx]};`);
      }
      const ids = await questions.reduce(async (_ids, question) => {
        let ids = await _ids; 
        if(
          !question.deleted && 
            question.text && 
            question.text.trim().length > 0 && 
            question.hueco && 
            question.hueco.trim().length > 0
        ) {
          const text = question.text.replace(/'/, "''");
          const hueco = question.hueco.replace(/'/, "''");
          let query;
          if(question.id) {
            query = `UPDATE questions SET text = '${text}', hueco = '${hueco}' where id = ${question.id} RETURNING id;`;
          } else {
            const maxExtantIdRes = await client.query('SELECT MAX(id) FROM questions;');
            if(maxExtantIdRes && maxExtantIdRes.rows && maxExtantIdRes.rows.length > 0) {
              const maxExtantId = maxExtantIdRes.rows[0].max;
              const nextvalRes = await client.query("SELECT nextval(pg_get_serial_sequence('questions', 'id'));");
              if(nextvalRes && nextvalRes.rows && nextvalRes.rows.length > 0) {
                const nextval = nextvalRes.rows[0].nextval;
                if(nextval <= maxExtantId) {
                  await client.query("SELECT setval(pg_get_serial_sequence('questions', 'id'), (SELECT MAX(id) FROM questions) + 1);");
                }
              }
            }
            query = `INSERT INTO questions (text, hueco) VALUES ('${text}', '${hueco}') RETURNING id;`;
          }
          console.log(`/api/savePromptWithQuestions > query for question is: ${query}`);
          const res = await client.query(query);
          if(res && res.rows && res.rows.length > 0) {
            ids.push(parseInt(res.rows[0].id));
          }
          return ids;
        } else {
          return ids;
        }
      }, []);
      console.log(`/api/savePromptWithQuestions > ids returned from questions is: ${JSON.stringify(ids)}`);
      await client.query(`UPDATE prompts SET question_ids = '{${ids.join(',')}}' where id = ${prompt.id};`);
      return new Response(JSON.stringify({ 
        success: `/api/savePromptWithQuestions > Prompt and questions saved, or if not, possibly in a parallel multiverse`,
        promptId: prompt.id
      }), { 
        headers: { 'Content-Type': 'application/json' }
      });
    } else {
      return new Response(JSON.stringify({ error: `/api/savePromptWithQuestions > Could not insert or update ${title} with id ${id}` }), { 
        headers: { 'Content-Type': 'application/json' }
      });
    }
  } catch(err) {
    console.log(`/api/savePromptWithQuestions > POST > we have a problem: ${err}`);
    return new Response(JSON.stringify({ error: `/api/savePromptWithQuestions > The universe has failed: ${err}` }), { 
      headers: { 'Content-Type': 'application/json' }
    });
  } finally {
    if(client) {
      await client.end();
    }
  }
};
