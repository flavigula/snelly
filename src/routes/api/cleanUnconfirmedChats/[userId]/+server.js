import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const userId = { params };
  if(userId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const fiveDaysFromNow = new Date(t.getTime() + 1000 * 3600 * 24 * 5);
      const day = fiveDaysFromNow.getUTCDate.toString().padStart(2, "0");
      const month = (fiveDaysFromNow.getUTCMonth() + 1).toString().padStart(2, "0");
      const pgTimeString = `${fiveDaysFromNow.getYear}-${month}-${day} 00:00:00`;
      res = await client.query(`DELETE FROM chats WHERE user_id = ${userId} AND confirmed = FALSE AND created_at > '${pgTimeString}';`);
      const status = { status: 'ok' };
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/cleanUnconfirmedChats > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/cleanUnconfirmedChats > POST > bad userId: ${userId}`,
      errorCode: 'no-chat-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
