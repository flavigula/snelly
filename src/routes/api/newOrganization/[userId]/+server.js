import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params, request }) => {
  const { userId } = params;
  const data = await request.formData();
  if(userId && parseInt(userId) !== NaN) {
    const name = data.get('name');
    const description = data.get('description');
    const longDescription = data.get('longDescription');
    if(name && name.length > 0) {
      let client;
      try {
        client = new Client(config.postgresOpts);
        await client.connect();
        const ordenRes = await client.query(`SELECT max(orden) from organizations where user_id = ${userId};`);
        let orden = 1;
        if(ordenRes && ordenRes.rows) {
          const maximum = parseInt(ordenRes.rows[0].max);
          if(maximum !== NaN) {
            orden = maximum + 1;
          }
        }
        const _description = (description && description.length > 0) ? description : '';
        const _longDescription = (longDescription && longDescription.length > 0) ? longDescription : '';
        await client.query(`INSERT INTO organizations(user_id, orden, name, description, long_description) VALUES($1, $2, $3, $4, $5);`, [userId, orden, name, _description, _longDescription]);
        return new Response(JSON.stringify({
          status: 'ok'
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
        
      } catch(err) {
        return new Response(JSON.stringify({
          error: `/api/newOrganization > POST > postgres err? ${err}`,
          errorCode: 'db'
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
      } finally {
        if(client) {
          await client.end();
        }
      }
    } else {
      return new Response(JSON.stringify({
        error: `/api/newOrganization > POST > no name`,
        errorCode: 'nameless'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/newOrganization > POST > no userId`,
      errorCode: 'no-user-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
});
