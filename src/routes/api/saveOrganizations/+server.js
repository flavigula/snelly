import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  // console.log('ENTERING');
  const data = await request.formData();
  const orgsString = data.get('organizations');
  // console.log(`/api/saveOrganizations > orgsString from formdata: ${JSON.stringify(orgsString)}`);
  let orgs = null;
  try {
    orgs = JSON.parse(orgsString);
  } catch(err) {
    console.log(`/api/saveOrganizations > ERROR: ${err}`);
    orgs = null;
  }
  // console.log(`/api/saveOrganizations > orgs from formdata: ${JSON.stringify(orgs)}`);
  if(orgs) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const remainder = orgs.filter(org => !org.removed && !org.modified && org.saved);
      await orgs.filter(org => org.removed).reduce(async (_nulu, org) => {
        const nulu = await _nulu;
        const deleteQuery = `DELETE FROM organizations WHERE id = '${org.id}' and user_id = ${org.userId}`;
        await client.query(deleteQuery);
        return nulu;
      }, null);
      let savedButModified = orgs.filter(org => org.modified && org.saved && !org.removed);
      savedButModified = await savedButModified.reduce(async (_orgs, org) => {
        const orgs = await _orgs;
        const updateQuery = `UPDATE organizations SET orden = $1, name = $2, description = $3, long_description = $4 WHERE id = ${org.id};`;
        const updateValues = [org.orden, org.name, org.description, org.longDescription];
        console.log(`/api/saveOrganizations > updateQuery: ${updateQuery}, updateValues: ${JSON.stringify(updateValues)}`);
        await client.query(updateQuery, updateValues);
        org.modified = false;
        org.saved = true;
        orgs.push(org);
        return orgs;
      }, []);
      console.log(`/api/saveOrganizations > savedButModified > ${JSON.stringify(savedButModified)}`);
      let notSavedAndNotRemoved = orgs.filter(org => !org.saved && !org.removed);
      notSavedAndNotRemoved = await notSavedAndNotRemoved.reduce(async (_orgs, org) => {
        const orgs = await _orgs;
        const insertQuery = `INSERT INTO organizations(user_id, orden, name, description, long_description) VALUES($1, $2, $3, $4, $5) RETURNING id;`;
        const insertValues = [org.userId, org.orden, org.name, org.description, org.longDescription];
        console.log(`/api/saveOrganizations > insertQuery: ${insertQuery}, insertValues: ${JSON.stringify(insertValues)}`);
        const res = await client.query(insertQuery, insertValues);
        if(res && res.rows && res.rows.length > 0) {
          org.id = res.rows[0].id;
        }
        org.modified = false;
        org.saved = true;
        orgs.push(org);
        return orgs;
      }, []);
      console.log(`/api/saveOrganizations > notSavedAndNotRemoved > ${JSON.stringify(notSavedAndNotRemoved)}`);
      const newOrgs = remainder.concat(savedButModified.concat(notSavedAndNotRemoved)).sort((a, b) => a.orden - b.orden);
      return new Response(JSON.stringify({
        status: 'ok',
        organizations: newOrgs
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/saveOrganizations > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/saveOrganizations > POST > no organizations to save?`,
      errorCode: 'no-organizations'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
