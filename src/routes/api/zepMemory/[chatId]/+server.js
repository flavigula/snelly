import { ZepClient } from '@getzep/zep-js';
import config from '$lib/config.js';

const timeout = 10; // try to find session for 10 seconds

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export const POST = async ({ params }) => {
  const { chatId } = params;
  if(chatId) {
    try {
      let messages = [];
      console.log(`/api/zepMemory > zepApiURL: ${config.zepApiURL}`);
      const zep = await ZepClient.init(config.zepApiURL, config.zepApiKey);
      console.log(`/api/zepMemory > POST > chatId : ${chatId}`);
      let memory;
      let interval;
      let iterations = 0;
      interval = setInterval(async () => {
        try {
          memory = await zep.memory.getMemory(chatId);
          console.log(`/api/zepMemory > POST > retrieved memory: ${JSON.stringify(memory)}`);
          clearInterval(interval);
          messages = (memory.messages || []).map(message => ({
            role: message.role,
            token_count: message.token_count,
            uuid: message.uuid,
            created_at: message.created_at
          }));
        } catch(err) {
          console.log(`/api/zepMemory > POST > error getting memory for ${chatId}: ${err}`);
          iterations = iterations + 1;
          if(iterations > timeout) {
            memory = "nulu";
            clearInterval(interval);
          }
        }
      }, 2000);
      for(let idx = 0; idx < timeout; idx++) {
        await sleep(1000);
        if(memory) break;
      }
      if(memory && memory !== "nulu") {
        return new Response(JSON.stringify({
          messages: messages
        }), {
            headers: {
              'Content-Type': 'application/json'
            }
          }
        );
      } else {
        console.log(`/api/zepMemory > POST > memory could not be found for ${chatId} after ${timeout} seconds`);
        return new Response(JSON.stringify({
          error: `/api/zepMemory > POST > memory could not be found for ${chatId} after ${timeout} seconds`
        }), {
            headers: {
              'Content-Type': 'application/json'
            }
          }
        );
      }
    } catch(err) {
      console.log(`/api/zepMemory > zep error? ${err}`);
      return new Response(JSON.stringify({
        error: `/api/zepMemory > POST > zep error? ${err}`
      }), {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/zepMemory > POST > bad data? chatId: ${chatId}`
    }), {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }
}
