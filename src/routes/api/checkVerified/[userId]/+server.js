import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId } = params;
  if(userId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(
        `SELECT is_free_trial, is_verified FROM users WHERE id = ${userId};`
      );
      let status = {};
      if(res && res.rows && res.rows.length === 1) {
        if(!res.rows[0].is_free_trial || res.rows[0].is_verified) {
          status = { status: 'ok' };
        } else {
          status = { error: 'unauthorized' };
        }
      } else {
        status = { error: `user id ${userId} not found` };
      }
      return new Response(JSON.stringify(status), {
        headers: { 'Content-Type': 'application/json' }
      });
    } catch(err) {
      console.log(`/checkVerified : ${err}`);
      return new Response(JSON.stringify({ error: err }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: 'No userId' }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
