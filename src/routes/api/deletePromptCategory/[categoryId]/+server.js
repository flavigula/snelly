import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { categoryId } = params;
  if(parseInt(categoryId) === NaN) {
    return new Response(JSON.stringify({
      error: `the category with id ${categoryId} had not been saved yet`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(`DELETE FROM prompt_categories WHERE id = ${categoryId} RETURNING id;`);
    if(res && res.rows && res.rows.length > 0) {
      id = res.rows[0].id;
    }
    return new Response(JSON.stringify({
      success: id
    }), { headers: { 'Content-Type': 'application/json' } });
  } catch(err) {
    return new Response(JSON.stringify({
      error: `/api/deletePromptCategory > POST > postgres err? ${err}`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
};
