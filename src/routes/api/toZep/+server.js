import { ZepClient, Memory, Message, Session, NotFoundError } from '@getzep/zep-js';
import config from '$lib/config.js';

export const POST = async ({ request }) => {
  const data = await request.formData();
  const username = data.get('username');
  const messagesStr = data.get('messages');
  const chatId = data.get('chatId');
  if(username && messagesStr && chatId)  {
    try {
      console.log(`/api/toZep > zepApiURL: ${config.zepApiURL}`);
      const zep = await ZepClient.init(config.zepApiURL, config.zepApiKey);
      let user;
      try {
        user = await zep.user.get(username);
      } catch(err) {
        if(err instanceof NotFoundError) {
          console.log(`/api/toZep > creating new user`);
          user = await zep.user.add({ user_id: username });
          console.log(`/api/toZep > user: ${JSON.stringify(user.toDict())}`);
        } else {
          throw err;
        }
      }
      let session;
      try {
        session = await zep.memory.getSession(chatId);
        console.log(`/api/toZep > POST > session found`);
      } catch(err) {
        if(err instanceof NotFoundError) {
          const _session = new Session({
            session_id: chatId,
            user_id: username,
            metadata: {}
          });
          session = await zep.memory.addSession(_session);
          console.log(`/api/toZep > POST > new session added`);
        } else {
          throw err;
        }
      }
      let _messages;
      try {
        _messages = JSON.parse(messagesStr);
      } catch(err) {
        console.log(`/api/toZep > POST > messages could not be parsed: ${JSON.stringify(messagesStr)}`);
        _messages = [];
      }
      // console.log(`/api/toZep > POST > messages before thurked to memory: ${JSON.stringify(_messages)}`);
      const messages = _messages.map(({ role, content }) => new Message({ role, content }));
      const memory = new Memory({ messages });
      // console.log(`/api/toZep > POST > new memory: ${JSON.stringify(memory.toDict())}`);
      await zep.memory.addMemory(chatId, memory);
      return new Response(JSON.stringify({
        success: "/api/toZep > POST > memory saved"
      }), {
          headers: {
            'Content-Type': "application/json"
          }
        }
      );
    } catch(err) {
      console.log(`/api/toZep/+server.js > POST > err: ${err}`);
      return new Response(JSON.stringify({
        error: `/api/toZep > POST > err: ${err}`}), {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      );
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/toZep > POST > bad data? username: ${username} messagesStr: ${messagesStr}`}), {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }
};
