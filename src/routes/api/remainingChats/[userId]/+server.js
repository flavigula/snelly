import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId } = params;
  if(userId && parseInt(userId) !== NaN) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(`SELECT chats_remaining from users where id = ${userId};`);
      if(res && res.rows && res.rows.length > 0) {
        return new Response(JSON.stringify({ status: 'ok', remainingChats: parseInt(res.rows[0].chats_remaining) }), {
          headers: { 'Content-Type': 'application/json' }
        });
      } else {
        return new Response(JSON.stringify({ status: 'error', remainingChats: 13 }), {
          headers: { 'Content-Type': 'application/json' }
        });
      }
    } catch(err) {
      return new Response(JSON.stringify({ error: `/api/remainingChats > postgres err? ${err}` }), {
        headers: { 'Content-Type': 'application/json' }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: `/api/remainingChats > no userId` }), {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
