import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  console.log('/api/saveAnswers > POST > entering...');
  const data = await request.formData();
  const userId = data.get('userId');
  const promptStr = data.get('prompt');
  const chatTitle = data.get('chatTitle');
  const questionsStr = data.get('questions');
  if(userId && promptStr && questionsStr) {
  let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      let prompt, questions;
      try {
        prompt = JSON.parse(promptStr);
        questions = JSON.parse(questionsStr);
        const uAGValues = [userId, prompt.id, prompt.title, chatTitle, new Date().getTime()];
        console.log(`/api/saveAnswers > POST > uAGValues: ${JSON.stringify(uAGValues)}`);
        const uAGRes = await client.query(
          `INSERT INTO user_answer_group(user_id, prompt_id, prompt_title, chat_title, created_at) VALUES($1, $2, $3, $4, $5) RETURNING id;`,
          uAGValues
        );
        let newId = -1;
        if(uAGRes && uAGRes.rows && uAGRes.rows.length > 0) {
          newId = uAGRes.rows[0].id;
        }
        if(newId > -1) {
          const _pQ = questions.map(question => {
            const values = [newId, prompt.id, question.id, question.answer, question.hueco];
            return client.query(
              `INSERT INTO user_answers(user_answer_group_id, prompt_id, question_id, answer, hueco) VALUES($1, $2, $3, $4, $5);`,
              values
            );
          });
          await Promise.all(_pQ);
          return new Response(JSON.stringify({
            success: `/api/saveAnswers > POST > id: ${newId}`,
            userAnswerGroupId: newId
          }), {
              headers: {
                'Content-Type': 'application/json'
              }
            });

        } else {
          return new Response(JSON.stringify({
            error: `/api/saveAnswers > POST > user_answer_group apparently not created`
          }), {
              headers: {
                'Content-Type': 'application/json'
              }
            });
        }
      } catch(err) {
        console.log(`/api/saveAnswers > ERR: ${err}`);
        console.log(`/api/saveAnswers > POST > bad prompt or questions: \nprompt: ${promptStr} \nquestions: ${questionsStr}`);
        return new Response(JSON.stringify({
          error: `/api/saveAnswers > POST > bad prompt or questions: ${promptStr} questions: ${questionsStr}`
        }), {
            headers: {
              'Content-Type': 'application/json'
            }
          });
      }
    } catch(err) {
      console.log(`/api/saveAnswers > POST > postgres err? ${err}`);
      return new Response(JSON.stringify({
        error: `/api/saveAnswers > POST > postgres err? ${err}`
      }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    } finally {
      if(client) {
        client.end();
      }
  }
  } else {
    console.log(`/api/saveAnswers > POST > bad data? userId: ${userId} promptStr: ${promptStr} questionsStr: ${questionsStr}`);
    return new Response(JSON.stringify({
      error: `/api/saveAnswers > POST > bad data? userId: ${userId} promptStr: ${promptStr} questionsStr: ${questionsStr}`
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
