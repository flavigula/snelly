import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  const data = await request.formData();
  const info = data.get('info');
  const userId = data.get('userId');
  if(info && userId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const encodedInfo = info.replace(/'/g, "''");
      const res = client.query(
        `INSERT INTO user_profiles (user_id, info) VALUES (${userId}, '${encodedInfo}') ON CONFLICT (user_id) DO UPDATE SET info = EXCLUDED.info RETURNING *;`
      );
      return new Response(JSON.stringify({
        success: 'ok'
      }), 
      {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `updateProfile > postgreserror? ${err}`
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    }
  } else {
    console.log(`/api/updateProfile > info: ${info}`);
    return new Response(JSON.stringify({ 
      error: 'updateProfile > No info or user id (for user profile) provided' 
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
