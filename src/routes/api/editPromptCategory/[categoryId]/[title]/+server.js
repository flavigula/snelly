import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { categoryId, title } = params;
  if(parseInt(categoryId) === NaN) {
    return new Response(JSON.stringify({
      error: `/api/editPromptCategory > POST > the category with id ${categoryId} had not valid`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
  if(!title) {
    return new Response(JSON.stringify({
      error: `/api/editPromptCategory > POST > no title`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(`UPDATE prompt_categories SET title = '${title}' where id = ${categoryId};`);
    if(res && res.rows && res.rows.length > 0) {
      id = res.rows[0].id;
    }
    return new Response(JSON.stringify({
      success: id
    }), { headers: { 'Content-Type': 'application/json' } });
  } catch(err) {
    return new Response(JSON.stringify({
      error: `/api/editPromptCategory > POST > postgres err? ${err}`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
};
