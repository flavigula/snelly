import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { username, token } = params;
  if(username && token) {
    // console.log(`/api/checkToken > POST > username: ${username} token: ${token}`);
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(
        'SELECT token FROM users WHERE username = $1',
        [username]
      );
      // console.log(`/api/checkToken > POST > res from query: ${JSON.stringify(res)}`);
      if(res && res.rows && res.rows.length > 0) {
        if(res.rows[0].token === token) {
          // console.log(`/api/checkToken > POST > returning SUCCESS`);
          return new Response(JSON.stringify({ success: 'ok' }), {
            headers: {
              'Content-Type': 'application/json'
            }
          });
        } else {
          return new Response(JSON.stringify({ error: 'Token incorrect' }), {
            headers: {
              'Content-Type': 'application/json'
            }
          });
        }
      } else {
        return new Response(JSON.stringify({ error: 'Username not found' }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
      }
    } catch(err) {
      // console.log(`/api/clearToken : ${err}`);
      return new Response(JSON.stringify({ error: err }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: 'No username or token or either' }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
