import { ChatOpenAI } from "langchain/chat_models/openai";
import { BytesOutputParser } from "langchain/schema/output_parser";
import { toZep } from '$lib/zepFromServer.js';

const model = new ChatOpenAI({
  openAIApiKey: import.meta.env.VITE_CHATGPT_API_KEY,
  temperature: 0.6
});

// const parser = new StringOutputParser();
const parser = new BytesOutputParser();

export const POST = async ({ request, fetch }) => {
  const data = await request.formData();
  const questionsStr = data.get('questions');
  const promptStr = data.get('prompt');
  const user = data.get('user');
  const chatId = data.get('chatId');
  console.log(`/api/firstPrompt > POST > chatId is: ${chatId}`);

  if(user && questionsStr && promptStr && chatId) {
    try {
      const prompt = JSON.parse(promptStr);
      const questions = JSON.parse(questionsStr);
      let huecos = questions.reduce((huecos, question) => {
        huecos[question.hueco] = question.answer;
        return huecos;
      }, {});
      huecos.format_instructions = "Format your output in two sections. The first section is the title. It is a summary of the rest of your response. The title HAS TO begin with ``` and end with ``` and can be no more than 12 words. After the title is a newline and then the rest of your response.";
      let userTemplate = prompt.user_template;
      let systemTemplate = `${prompt.system_template}\n{format_instructions}`;
      userTemplate = Object.keys(huecos).reduce((userTemplate, key) => {
        console.log(`/api/firstPrompt > Replacing {${key}} with ${huecos[key]}`);
        const re = new RegExp(`\{${key}\}`, "i");
        userTemplate = userTemplate.replace(re, huecos[key]); 
        return userTemplate;
      }, userTemplate);
      systemTemplate = Object.keys(huecos).reduce((systemTemplate, key) => {
        const re = new RegExp(`\{${key}\}`, "i");
        systemTemplate = systemTemplate.replace(re, huecos[key]); 
        return systemTemplate;
      }, systemTemplate);
      console.log(`/api/firstPrompt > userTemplate: ${userTemplate}`);
      console.log(`/api/firstPrompt > systemTemplate: ${systemTemplate}`);
      /*
      const chain = new ConversationChain({ llm: model, outputParser: parser, prompt: [
        ["system", systemTemplate],
        ["human", userTemplate]
      ], memory });
      */
      console.log(`CREATED CONVERSATION CHAIN`);
      /*
      const stream = await chain.stream([
        ["system", systemTemplate],
        ["human", userTemplate]
      ]);
      */
      // const stream = await chain.stream();
      // console.log(`CREATED STREAM`);
      /*
      for await (const chunk of stream) {
        console.log(chunk);
      }
      */
      const stream = await model.pipe(parser).stream([
        ["system", systemTemplate],
        ["human", userTemplate]
      ]);
      const [ toZepStream, toBrowserStream ] = stream.tee();
      toZep(fetch, user, [
        { role: "system", content: systemTemplate },
        { role: "human", content: userTemplate }
      ], chatId, toZepStream);

      console.log(`/api/firstPrompt > returning the stream`);
      return new Response(toBrowserStream, {
        headers: {
          'Content-Type': 'text/plain; charset=utf-8'
        }
      });
    } catch(err) {
      console.log(`/api/firstPrompt > ERR: ${err}`);
      console.log(`/api/firstPrompt > POST > bad user or prompt or questions: \n user: ${user}\nprompt: ${promptStr} \nquestions: ${questionsStr}`);
      return new Response(JSON.stringify({
        error: `/api/firstPrompt > POST > bad user or prompt or questions: user: ${user} ${promptStr} questions: ${questionsStr}`
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/firstPrompt > POST > bad data? userStr: ${userStr} promptStr: ${promptStr} questionsStr: ${questionsStr}`
    }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }
};
