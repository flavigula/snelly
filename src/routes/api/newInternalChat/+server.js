import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  console.log('/api/newInternalChat > ENTERING');
  const data = await request.formData();
  const organizationIdStr = data.get('organizationId');
  const promptIdStr = data.get('promptId');
  const userIdStr = data.get('userId');
  const title = data.get('title') || 'Untitled';
  const organizationId = parseInt(organizationIdStr);
  const promptId = parseInt(promptIdStr);
  const userId = parseInt(userIdStr);
  console.log(`/api/newInternalChat > promptId: ${promptId}, userId: ${userId}, organizationId: ${organizationId}`);
  if(![ promptId, userId ].some(a => isNaN(a))) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const columns = `title, ${isNaN(organizationId) ? '' : ('organization_id' + ', ') }prompt_id, user_id, created_at`;
      const values = isNaN(organizationId) ? "$1, $2, $3, $4" : "$1, $2, $3, $4, $5";
      const insertQuery = `INSERT INTO chats(${columns}) VALUES(${values}) RETURNING id;`;
      let insertValues = [ title ];
      insertValues = insertValues.concat(isNaN(organizationId) ? [] : [ organizationId ]);
      insertValues = insertValues.concat([ promptId, userId, new Date() ]);
      console.log(`/api/newInternalChat > insertQuery: ${insertQuery}, insertValues: ${JSON.stringify(insertValues)}`);
      const res = await client.query(insertQuery, insertValues);
      let chatId = null; // for reference
      if(res && res.rows && res.rows.length > 0) {
        chatId = parseInt(res.rows[0].id);
        if(isNaN(chatId)) chatId = null;
      }
      let status = { chatId };
      if(chatId) {
        status.status = 'ok';
      } else {
        status.error = '/api/newInternalChat > postgres did not return an id for the new chat';
      }
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      console.log(`/api/newInternalChat > err: ${err}`);
      return new Response(JSON.stringify({
        error: `/api/newInternalChat > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/newInternalChat > POST > bad userId: ${userId} or promptId: ${promptId}`,
      errorCode: 'no-user-id-or-prompt-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
