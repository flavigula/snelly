import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId } = params;
  if(userId && parseInt(userId) !== NaN) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(`SELECT prompt_category_id from users_categories_favourites where user_id = ${userId}`);
      if(res && res.rows) {
        const catIds = res.rows.map(row => row.prompt_category_id);
        return new Response(JSON.stringify({
          success: catIds
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });

      } else {
        return new Response(JSON.stringify({
          error: `/api/fetchFavourites > POST > postgres did not return any rows`
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
      }
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/fetchFavourites > POST > postgres err? ${err}`
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/fetchFavourites > POST > no userId`
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
