import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;
import { templateType } from '$lib/store/Store.svelte';

export const POST = async({ params }) => {
  let tipo;
  try {
    tipo = templateType(params.tipo);
  } catch(err) {
    console.log(`/api/fetchTemplatesByType > POST > type error: ${params.tipo}`);
  }
 
  if(tipo) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(
        `SELECT * from templates where tipo = $1;`, 
        [ tipo ]
      ); 
      if(res && res.rows) {
        const templates = res.rows.map(row => {
          return {
            id: row.id,
            title: row.title,
            questionIds: row.question_ids,
            template: row.template,
            tipo: row.tipo
          };
        });
        const status = {
          status: 'ok',
          templates
        };
        console.log(`/api/fetchTemplatesByType > status: ${JSON.stringify(status)}`);
        return new Response(JSON.stringify(status), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
      } else {
        return new Response(JSON.stringify({
          status: 'error',
          error: '/api/fetchTemplatesByType > POST > postgres returned nothing'
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
      }
    } catch(err) {
      console.log(`/api/fetchTemplatesByType > POST > postgres err? ${err}`);
      return new Response(JSON.stringify({
        error: `/api/fetchTemplatesByType > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/fetchTemplatesByType > POST > incorrect type: ${JSON.stringify(params)}`,
      errorCode: 'incorrect-template-type'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
