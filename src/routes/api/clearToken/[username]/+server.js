import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { username } = params;
  if(username) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      await client.query(
        'UPDATE users set token = NULL where username = $1',
        [username]
      );
      return new Response(JSON.stringify({ success: "ok" }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      console.log(`/api/clearToken : ${err}`);
      return new Response(JSON.stringify({ error: err }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: 'No username' }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
