import { ChatOpenAI } from "langchain/chat_models/openai";
import { BytesOutputParser } from "langchain/schema/output_parser";
import { toZep } from '$lib/zepFromServer.js';

const model = new ChatOpenAI({
  openAIApiKey: import.meta.env.VITE_CHATGPT_API_KEY,
  temperature: 0.6
});

// const parser = new StringOutputParser();
const parser = new BytesOutputParser();

export const POST = async ({ request, fetch }) => {
  const data = await request.formData();
  const messagesStr = data.get('messages');
  const promptStr = data.get('prompt');
  const user = data.get('user');
  const chatId = data.get('chatId');

  if(messagesStr && promptStr) {
    try {
      const messages = JSON.parse(messagesStr);
      const prompt = JSON.parse(promptStr);

      let _messages = [['system', prompt.system_template]].concat(
        messages.map(message => {
          return [message.role, message.content];
        })
      );
      const stream = await model.pipe(parser).stream(_messages);
      const [ toZepStream, toBrowserStream ] = stream.tee();
      let content = '';
      if(messages && messages.length > 0) {
        const message = messages.findLast(m => m.role === 'human' || m.role === 'user');
        if(message) {
          content = message.content || '';
        }
      }
      toZep(fetch, user, [ { role: "human", content } ], chatId, toZepStream);
      console.log(`/api/sendMessages > returning the stream`);
      return new Response(toBrowserStream, {
        headers: {
          'Content-Type': 'text/plain; charset=utf-8'
        }
      });
    } catch(err) {
      console.log(`/api/sendMessages > ERR: ${err}`);
      console.log(`/api/sendMessages > POST > bad messages or prompt: \n messages: ${messagesStr} prompt: ${promptStr}`);
      return new Response(JSON.stringify({
        error: `/api/sendMessages > POST > bad messages or prompt: messages: ${messagesStr} prompt: ${promptStr}`
        }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/sendMessages > POST > bad data? messages: ${messagesStr} prompt: ${promptStr}`
    }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }
};
