import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { internalChatId } = params;
  if(internalChatId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const updateQuery = `UPDATE chats set removed = TRUE where id = ${internalChatId};`;
      await client.query(updateQuery);
      const status = { status: 'ok' };
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/removeChat > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/removeChat > POST > bad internalChatId: ${internalChatId}`,
      errorCode: 'no-internal-chat-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
