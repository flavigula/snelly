import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ request }) => {
  const data = await request.formData();
  let client;
  try {
    const id = data.get('id');
    const promptCategoryIds = JSON.parse(data.get('prompt_category_ids'));
    const title = data.get('title').replace(/'/g, "''");
    const questionIds = JSON.parse(data.get('question_ids'));
    const systemTemplate = data.get('system_template').replace(/'/g, "''");
    const userTemplate = data.get('user_template').replace(/'/g, "''");
    client = new Client(config.postgresOpts);
    await client.connect();
    const pCIds = promptCategoryIds.map(id => `${id}`).join(',');
    const qIds = questionIds.map(id => `${id}`).join(',');
    // const templatesRes = await client.query(`UPDATE templates set template = '${systemTemplate}' where id = 
    const res = await client.query(`UPDATE prompts SET prompt_category_ids = '{${pCIds}}', title = '${title}', question_ids = '{${qIds}}', system_template = '${systemTemplate}', user_template = '${userTemplate}' where id = ${id} RETURNING *;`);
    if(res && res.rows && res.rows.length > 0) {
      return new Response(JSON.stringify({ success: res.rows[0] }), { 
        headers: { 'Content-Type': 'application/json' }
      });
    } else {
      return new Response(JSON.stringify({ error: '/api/savePrompts > nothing returned' }), { 
        headers: { 'Content-Type': 'application/json' }
      });
    }
  } catch(err) {
    console.log(`/api/savePrompts > POST > we have a problem: ${err}`);
    return new Response(JSON.stringify({ error: `/api/savePrompts > something went wrong: ${err}` }), { 
      headers: { 'Content-Type': 'application/json' }
    });
  } finally {
    if(client) {
      await client.end();
    }
  }
};
