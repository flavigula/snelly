import pg from 'pg';
import config from '$lib/config.js';
import { v4 as uuidv4 } from 'uuid';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId } = params;
  if(userId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const verificationToken = uuidv4();
      const vtExpiry = new Date().getTime() + (3600 * 24 * 1000); // One day
      // const vtExpiry = new Date().getTime() + (60 * 2 * 1000); // Two minutes
      const res = await client.query(
        `UPDATE users set is_verified = FALSE, verification_token = '${verificationToken}', verification_token_expiry = ${vtExpiry} where id = ${userId} returning *;`
      );
      let status = {};
      if(res && res.rows && res.rows.length === 1) {
        status = { 
          status: 'ok',
          user: res.rows[0]
        };
      } else {
        status = { error: `user id ${userId} not found` };
      }
      return new Response(JSON.stringify(status), {
        headers: { 'Content-Type': 'application/json' }
      });
    } catch(err) {
      console.log(`/checkVerified : ${err}`);
      return new Response(JSON.stringify({ error: err }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: 'No userId' }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
