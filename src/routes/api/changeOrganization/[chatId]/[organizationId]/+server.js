import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { chatId, organizationId } = params;
  if(chatId) {
    const orgId = parseInt(organizationId);
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const updateQuery = `UPDATE chats set organization_id = ${(isNaN(orgId) || orgId === 0) ? 'NULL' : orgId} where id = ${chatId};`;
      console.log(`/api/changeOrganization > updateQuery: ${updateQuery}`);
      await client.query(updateQuery);
      const status = { status: 'ok' };
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      console.log(`/api/changeOrganization > err: ${err}`);
      return new Response(JSON.stringify({
        error: `/api/changeOrganization > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/newInternalChat > POST > bad chatId: ${chatId} or organizationId: ${organizationId}`,
      errorCode: 'no-chat-id-or-organization-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
