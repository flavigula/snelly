import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params, request }) => {
  const data = await request.formData();
  const title = data.get('title');
  const { internalChatId } = params;
  if(internalChatId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const updateQuery = `UPDATE chats set confirmed = TRUE, title = $1 where id = ${internalChatId};`;
      const updateValues = [ title ];
      await client.query(updateQuery, updateValues);
      const status = { status: 'ok' };
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/confirmChat > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/confirmChat > POST > bad internalChatId: ${internalChatId}`,
      errorCode: 'no-internal-chat-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
