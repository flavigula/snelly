import Stripe from 'stripe';

const calculateOrderAmount = (items) => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client
  return 0.5;
};

export const POST = async ({ request }) => {
  const stripe = Stripe(import.meta.env.VITE_STRIPE_SECRET_KEY);
  const { items } = request.body || [];

  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: 50, // calculateOrderAmount(items),
    currency: "eur",
    // In the latest version of the API, specifying the `automatic_payment_methods` parameter is optional because Stripe enables its functionality by default.
    automatic_payment_methods: {
      enabled: true,
    },
  });

  console.log(`/api/createPaymentIntent > client_secret: ${JSON.stringify(paymentIntent.client_secret)}`);

  return new Response(JSON.stringify({
    clientSecret: paymentIntent.client_secret,
  }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
};
