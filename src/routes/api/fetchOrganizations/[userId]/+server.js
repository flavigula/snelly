import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  console.log(`/api/fetchOrganizations > ENTERING`);
  const { userId } = params;
  if(userId && parseInt(userId) !== NaN) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      let orgs = [];
      const orgsRes = await client.query(`SELECT * from organizations where user_id = ${userId} ORDER BY orden;`);
      // console.log(`/api/fetchOrganizations > orgsRes: ${JSON.stringify(orgsRes)}`);
      if(orgsRes && orgsRes.rows) {
        orgs = await orgsRes.rows.reduce(async (_orgs, orgRow) => {
          let orgs = await _orgs;
          let removedChats = 0;
          let extantChats = 0;
          const removedRes = await client.query(`SELECT count(*) FROM chats WHERE removed = TRUE and organization_id = ${orgRow.id};`);
          if(removedRes && removedRes.rows && removedRes.rows.length > 0) {
            removedChats = removedRes.rows[0].count;
          }
          const extantRes = await client.query(`SELECT count(*) FROM chats WHERE removed = FALSE and organization_id = ${orgRow.id};`);
          if(extantRes && extantRes.rows && extantRes.rows.length > 0) {
            extantChats = extantRes.rows[0].count;
          }
          orgRow.saved = true;
          orgRow.modified = false;
          orgRow.removedChats = removedChats;
          orgRow.extantChats = extantChats;
          orgRow.userId = orgRow.user_id;
          orgRow.longDescription = orgRow.long_description;
          orgs.push(orgRow);
          return orgs;
        }, []);
        console.log(`/api/fetchOrganizations > orgs: ${JSON.stringify(orgs)}`);
      }
      return new Response(JSON.stringify({
        status: 'ok',
        organizations: orgs
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/fetchOrganizations > POST > postgres err? ${err}`,
        errorCode: 'db'
      }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/fetchOrganizations > POST > no userId`,
      errorCode: 'no-user-id'
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
