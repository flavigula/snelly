import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId } = params;
  if(userId && parseInt(userId) !== NaN) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const res = await client.query(`SELECT * FROM users WHERE id = ${userId} AND is_admin = true;`);
      if(res && res.rows && res.rows.length > 0) {
        return new Response(JSON.stringify({ success: 'ok' }), {
          headers: { 'Content-Type': 'application/json' }
        });
      } else {
        return new Response(JSON.stringify({ failure: 'not admin' }), {
          headers: { 'Content-Type': 'application/json' }
        });
      }
    } catch(err) {
      return new Response(JSON.stringify({ error: `/api/isAdmin > postgres err? ${err}` }), {
        headers: { 'Content-Type': 'application/json' }
      });
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return new Response(JSON.stringify({ error: `/api/isAdmin > no userId` }), {
      headers: { 'Content-Type': 'application/json' }
    });
  }
};
