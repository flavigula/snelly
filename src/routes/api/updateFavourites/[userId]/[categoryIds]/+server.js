import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { userId, categoryIds } = params;
  if(userId && categoryIds) {
    const uid = parseInt(userId);
    const catIds = categoryIds.split(/,/).map(catId => parseInt(catId)).filter(catId => catId !== NaN);
    if(uid !== NaN) {
      let client;
      try {
        client = new Client(config.postgresOpts);
        await client.connect();
        await client.query(`DELETE FROM users_categories_favourites WHERE user_id = ${uid};`);
        const values = catIds.map(catId => {
          return `(${uid}, ${catId})`;
        }).join(", ");
        const insertQuery = `INSERT INTO users_categories_favourites(user_id, prompt_category_id) VALUES ${values};`
        console.log(`/api/updateFavourites > POST > insertQuery: ${insertQuery}`);
        await client.query(insertQuery);
        return new Response(JSON.stringify({
          success: 'ok'
        }), {
            headers: {
              'Content-Type': 'application/json'
            }
          });
         
      } catch(err) {
        return new Response(JSON.stringify({
          error: `/api/updateFavourites > POST > postgres err? ${err}`
        }), {
            headers: {
              'Content-Type': 'application/json'
            }
          });
      } finally {
        if(client) {
          await client.end();
        }
      }
    } else {
      return new Response(JSON.stringify({
        error: `/api/updateFavourites > POST > malformed userId`
      }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/updateFavourites > POST > no userId and / or categoryIds`
    }), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
};
