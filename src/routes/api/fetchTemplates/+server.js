import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;
import { templateType } from '$lib/store/Store.svelte';

export const POST = async() => {
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    const res = await client.query(`SELECT * from templates;`); 
    if(res && res.rows) {
      const templates = res.rows.map(row => {
        return {
          title: row.title,
          questionIds: row.question_ids,
          text: row.text,
          tipo: row.tipo
        };
      });
      const status = {
        status: 'ok',
        templates
      };
      return new Response(JSON.stringify(status), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } else {
      return new Response(JSON.stringify({
        status: 'error',
        error: '/api/fetchTemplates > POST > postgres returned nothing'
      }), {
          headers: {
            'Content-Type': 'application/json'
          }
        });
    }
  } catch(err) {
    console.log(`/api/fetchTemplates > POST > postgres err? ${err}`);
    return new Response(JSON.stringify({
      error: `/api/fetchTemplates > POST > postgres err? ${err}`,
      errorCode: 'db'
    }), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  } finally {
    if(client) {
      await client.end();
    }
  }
};
