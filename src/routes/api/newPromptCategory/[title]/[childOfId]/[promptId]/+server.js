import pg from 'pg';
import config from '$lib/config.js';
const { Client } = pg;

export const POST = async ({ params }) => {
  const { title, childOfId, promptId } = params;
  console.log(`/api/newPromptCategory > POST > title: ${title}, childOfId: ${childOfId}, promptId: ${promptId}`);
  if(title) {
    let child_of_id, prompt_id;
    if(childOfId && childOfId !== "null" && parseInt(childOfId) !== NaN) {
      child_of_id = parseInt(childOfId);
    }
    if(promptId && promptId !== "null" && parseInt(promptId) !== NaN) {
      prompt_id = parseInt(promptId);
    }
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      let childOfColumn = '';
      let promptIdColumn = '';
      let childOfValue = '';
      let promptIdValue = '';
      if(child_of_id) {
        childOfColumn = ', child_of_id';
        childOfValue = `, ${child_of_id}`;
      }
      if(prompt_id) {
        promptIdColumn = ', prompt_id';
        promptIdValue = `, ${prompt_id}`;
      }
      let id;
      const res = await client.query(`INSERT INTO prompt_categories(title${childOfColumn}${promptIdColumn}) VALUES('${title}'${childOfValue}${promptIdValue}) RETURNING id;`);
      if(res && res.rows && res.rows.length > 0) {
        id = res.rows[0].id;
      }
      return new Response(JSON.stringify({
        success: id
      }), { headers: { 'Content-Type': 'application/json' } });
    } catch(err) {
      return new Response(JSON.stringify({
        error: `/api/newPromptCategory > POST > postgres err? ${err}`
      }), { headers: { 'Content-Type': 'application/json' } });
    }
  } else {
    return new Response(JSON.stringify({
      error: `/api/newPromptCategory > POST > no title given`
    }), { headers: { 'Content-Type': 'application/json' } });
  }
};
