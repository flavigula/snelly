import pg from 'pg';
import config from '$lib/config.js';

const { Client } = pg;
export const load = async ({ params }) => {
  const { promptId } = params;
  console.log(`/(app)/chat/snek/[promptId]/+page.server.js > promptId: ${promptId}`);
  if(promptId) {
    let client;
    try {
      client = new Client(config.postgresOpts);
      await client.connect();
      const promptQuery = `SELECT p.id, p.title, st.template AS system_template, ut.template AS user_template from prompts p, templates st, templates ut where p.id = ${promptId} AND st.id = p.system_template_id AND ut.id = p.user_template_id;`;
      const questionsQuery = `SELECT questions.id, questions.text, questions.hueco from prompts, questions where prompts.id = ${promptId} AND questions.id = ANY(prompts.question_ids);`
      const promptRes = await client.query(promptQuery, []);
      let prompt;
      if(promptRes && promptRes.rows && promptRes.rows.length > 0) {
        prompt = promptRes.rows[0];
      }
      console.log(`/(app)/chat/snek/[promptId]/+page.server.js > prompt: ${JSON.stringify(prompt)}`);
      const questionsRes = await client.query(questionsQuery, []);
      let questions = [];
      if(questionsRes && questionsRes.rows && questionsRes.rows.length > 0) {
        questions = questionsRes.rows;
      }
      // console.log(`/(app)/chat/snek/[promptId]/+page.server.js > questions: ${JSON.stringify(questions)}`);
      return {
        prompt, questions
      };
    } catch(err) {
      console.log(`/(app)/chat/snek/[promptId]/+page.server.js > error: ${err}`);
      return {
        error: "Postscript error?"
      };
    } finally {
      if(client) {
        await client.end();
      }
    }
  } else {
    return {
      error: "No promptId"
    };
  }
};
