import pg from 'pg';
import config from '$lib/config.js';
import { recurse } from '$lib/utils.js';
const { Client } = pg;

export const load = async () => {
  let client;
  try {
    client = new Client(config.postgresOpts);
    await client.connect();
    let allCategories = [];
    const res = await client.query(
      'SELECT * FROM prompt_categories;'
    );
    if(res && res.rows && res.rows.length > 0) {
      allCategories = res.rows;
      const categories = res.rows.filter(row => row.child_of_id === null).map(row => recurse(res.rows, row));
      // console.log(`/(app)/+layout.server.js > load > categories: ${JSON.stringify(categories)}`);
      return {
        categories,
        allCategories
      };
    } else {
      return { categories: [] };
    }
  } catch(err) {
    console.log(`/(app)/+layout.server.js > load > a problem? ${err}`);
    return { categories: [] };
  } finally {
    if(client) {
      await client.end();
    }
  }
}
