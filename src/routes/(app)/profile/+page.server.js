export const actions = {
  updateProfile: async ({ request }) => {
    const data = await request.formData();
    console.log(`/(app)/profile/+page.server.js > updateProfile > info: ${data.get('info')}`);
  }
};
