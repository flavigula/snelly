'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.removeColumn('prompts', 'system_template'));
  _p.push(db.removeColumn('prompts', 'assistant_template'));
  _p.push(db.addColumn(
    'prompts', 'system_template_id', 
    { 
      type: 'int',
      foreignKey: {
        name: 'prompts_system_template_id_fk',
        table: 'templates',
        rules: {
          onDelete: 'SET NULL'
        },
        mapping: 'id'
      }
    }
  ));
  _p.push(db.addColumn(
    'prompts', 'user_template_id', 
    { 
      type: 'int',
      foreignKey: {
        name: 'prompts_user_template_id_fk',
        table: 'templates',
        rules: {
          onDelete: 'SET NULL'
        },
        mapping: 'id'
      }
    }
  ));
  return Promise.all(_p); 
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.removeColumn('prompts', 'user_template_id'));
  _p.push(db.removeColumn('prompts', 'system_template_id'));
  _p.push(db.addColumn('system_template', { type: 'text' }));
  _p.push(db.addColumn('assistant_template', { type: 'text' }));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
