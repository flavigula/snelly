'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.removeColumn('prompts', 'created_at'));
  _p.push(db.removeColumn('prompts', 'updated_at'));
  _p.push(db.addColumn('prompts', 'created_at', { type: 'bigint' }));
  _p.push(db.addColumn('prompts', 'updated_at', { type: 'bigint' }));
  _p.push(db.removeColumn('prompts', 'template'));
  _p.push(db.addColumn('prompts', 'system_template', { type: 'text' }));
  _p.push(db.addColumn('prompts', 'assistant_template', { type: 'text' }));
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.removeColumn('prompts', 'system_template'));
  _p.push(db.removeColumn('prompts', 'assistant_template'));
  _p.push(db.addColumn('prompts', 'template', { type: 'text' }));
  _p.push(db.removeColumn('prompts', 'created_at'));
  _p.push(db.removeColumn('prompts', 'updated_at'));
  _p.push(db.addColumn('prompts', 'created_at', { type: 'timestamp' }));
  _p.push(db.addColumn('prompts', 'updated_at', { type: 'timestamp' }));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
