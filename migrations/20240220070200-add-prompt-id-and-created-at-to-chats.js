'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.addColumn('chats', 'prompt_id', { type: 'int' }));
  _p.push(db.addColumn('chats', 'created_at', { type: 'timestamp' }));
  _p.push(db.renameColumn('chats', 'name', 'title'));
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.renameColumn('chats', 'title', 'name'));
  _p.push(db.removeColumn('chats', 'created_at'));
  _p.push(db.removeColumn('chats', 'prompt_id'));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
