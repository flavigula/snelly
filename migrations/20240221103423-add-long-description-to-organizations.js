'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const res = db.addColumn('organizations', 'long_description', { type: 'text', defaultValue: '' });
  return res;
};

exports.down = function(db) {
  const res = db.removeColumn('organizations', 'long_description');
  return res;
};

exports._meta = {
  "version": 1
};
