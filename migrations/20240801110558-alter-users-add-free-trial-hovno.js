'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.addColumn('users', 'is_free_trial', { type: 'boolean', default: true }));
  _p.push(db.addColumn('users', 'free_trial_expiry', { type: 'bigint' }));
  _p.push(db.addColumn('users', 'chats_remaining', { type: 'int', defaultValue: 15 }));
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.removeColumn('users', 'is_free_trial'));
  _p.push(db.removeColumn('users', 'free_trial_expiry'));
  _p.push(db.removeColumn('users', 'chats_remaining'));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
