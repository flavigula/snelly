'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  console.log(`users`);
  const res = db.createTable('users', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    username: 'string',
    password: 'string',
    name: 'string',
    token: 'string',
    expiry: 'timestamp',
    created_at: 'timestamp',
    updated_at: 'timestamp'
  });
  return res;
};

exports.down = function(db) {
  const res = db.dropTable('users', { ifExists: true });
  return res;
};

exports._meta = {
  "version": 1
};
