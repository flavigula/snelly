'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const res = db.createTable('chats', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    name: { type: 'string', defaultValue: 'none' },
    organization_id: 'int',
    user_id: 'int'
  });
  return res;
};

exports.down = function(db) {
  const res = db.dropTable('chats', { ifExists: true });
  return res;
};

exports._meta = {
  "version": 1
};
