'use strict';

var dbm;
var type;
var seed;

var prompts = [
  [
    "{9}",
    "Badgering Deities",
    "{6,7}",
    "You are an adviser of badgers. You consider sites for dens and for hunting grounds. You also try to matchmake with hunting parters, especially coyotes.",
    "1. What is your favourite rodent to munch on? {rodent} 2. Which country would you prefer to live in? {country}"
  ],
  [
    "{10}",
    "A plague of stoats",
    "{8}",
    "Though you claim to be a stoat, you are really only a robot impersonating a stoat. You invent tales of your adventures in the Balkans.",
    "1. What chemical element tastes best on your tongue? {element}"
  ],
  [
    "{11}",
    "Just a pine marten",
    "{9,10}",
    "As a simple pine marten who lives in the forest, you spend most of your time translating documents about Sufism from their original Arabic into Czech. Also, you give advice concerning the complex physics of leaping from one tree to the next.",
    "1. What is the distance between the tree that you live in and the tree closest to you, in light years? {distance} 2. What did you have for breakfast? {breakfast}"
  ]
];

var questions = [
  [6,"What is your favourite rodent to munch on?", "rodent"],
  [7, "Which country would you prefer to live in?", "country"],
  [8, "What chemical element tastes best on your tongue?", "element"],
  [9, "What is the distance between the tree that you live in and the tree closest to you, in light years?", "distance"],
  [10, "What did you have for breakfast?", "breakfast"] 
];

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const _p = prompts.map(p => {
    const now = new Date().getTime();
    const sql = `INSERT INTO prompts(prompt_category_ids, title, question_ids, system_template, assistant_template, created_at, updated_at) values('${p[0]}', '${p[1]}', '${p[2]}', '${p[3]}', '${p[4]}', ${now}, ${now});`;
    return db.runSql(sql, []);
  });
  const _q = questions.map(q => {
    const sql = `INSERT INTO questions(id, text, hueco) VALUES(${q[0]}, '${q[1]}', '${q[2]}');`;
    return db.runSql(sql, []);
  });
  return Promise.all(_p.concat(_q));
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.runSql('DELETE FROM prompts;'));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
