'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const res = db.addColumn('user_answers', 'chat_id', { type: 'int' });
  return res;
};

exports.down = function(db) {
  const res = db.removeColumn('user_answers', 'chat_id');
  return res;
};

exports._meta = {
  "version": 1
};
