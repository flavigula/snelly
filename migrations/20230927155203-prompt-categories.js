'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  console.log(`categories`);
  const res = db.createTable('prompt_categories', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    title: 'string',
    child_of_id: 'int',
    created_at: 'timestamp',
    updated_at: 'timestamp'
  });
  return res;
};

exports.down = function(db) {
  const res = db.dropTable('prompt_categories', { ifExists: true });
  return res;
};

exports._meta = {
  "version": 1
};
