'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.removeColumn('users', 'expiry'));
  _p.push(db.addColumn('users', 'expiry', { type: 'bigint' }));
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.removeColumn('users', 'expiry'));
  _p.push(db.addColumn('users', 'expiry', { type: 'timestamp' }));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
