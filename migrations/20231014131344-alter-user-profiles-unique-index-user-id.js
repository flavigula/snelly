'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const res = db.addIndex('user_profiles', 'user_id_index', 'user_id', true);
  return res;
};

exports.down = function(db) {
  const res = db.removeIndex('user_profiles', 'user_id_index');
  return res;
};

exports._meta = {
  "version": 1
};
