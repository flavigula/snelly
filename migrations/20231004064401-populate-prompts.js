'use strict';

var dbm;
var type;
var seed;

var prompts = [
  [
    "{2}",
    "Help Me Write a Blog Article",
    "{1,2,3,4,5}",
    "You are a marketing expert. Take the answers to the questions into consideration and write me the best possible blog article you can starting with a title and then the article body making sure not to exceed the word limit. If there is no word limit, restrict the length of the article to 500 words. Do not provide any comments or explanation, only the title and the article text.",
    "1. What should your blog article be about? {about} 2. How many words should your article be? {word-count} 3. Should there be anything specific mentioned? {specifics} 4. What is the target audience? {audience} 5. What keywords should be included in the article? {keywords}"
  ]
];

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const _p = prompts.map(p => {
    const now = new Date().getTime();
    const sql = `INSERT INTO prompts(prompt_category_ids, title, question_ids, system_template, assistant_template, created_at, updated_at) values('${p[0]}', '${p[1]}', '${p[2]}', '${p[3]}', '${p[4]}', ${now}, ${now});`;
    return db.runSql(sql, []);
  });
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.runSql('DELETE FROM prompts;'));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
