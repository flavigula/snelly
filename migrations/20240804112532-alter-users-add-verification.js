'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let _p = [];
  _p.push(db.addColumn('users', 'verification_token', { type: 'string' }));
  _p.push(db.addColumn('users', 'is_verified', { type: 'boolean', defaultValue: false }));
  return Promise.all(_p);
};

exports.down = function(db) {
  let _p = [];
  _p.push(db.removeColumn('users', 'verification_token'));
  _p.push(db.removeColumn('users', 'is_verified'));
  return Promise.all(_p);
};

exports._meta = {
  "version": 1
};
