'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  const res = db.createTable('prompts', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    prompt_category_ids: { type: 'int[]' },
    title: 'string',
    question_ids: { type: 'int[]' },
    template: 'text',
    created_at: 'timestamp',
    updated_at: 'timestamp'
  });
  return res;
};

exports.down = function(db) {
  const res = db.dropTable('prompts', { ifExists: true });
  return res;
};

exports._meta = {
  "version": 1
};
