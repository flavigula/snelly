// vite.config.ts
import { sveltekit } from "file:///home/polaris/melampus/archiv/rummaging_round/node.js/snelly/node_modules/@sveltejs/kit/src/exports/vite/index.js";
import { defineConfig } from "file:///home/polaris/melampus/archiv/rummaging_round/node.js/snelly/node_modules/vite/dist/node/index.js";
import dsv from "file:///home/polaris/melampus/archiv/rummaging_round/node.js/snelly/node_modules/@rollup/plugin-dsv/dist/es/index.js";
import purgecss from "file:///home/polaris/melampus/archiv/rummaging_round/node.js/snelly/node_modules/@fullhuman/postcss-purgecss/lib/postcss-purgecss.js";
var plugins = [sveltekit(), dsv()];
var server = {
  port: 5005
};
var css = {
  postcss: {
    plugins: [
      purgecss({
        content: ["./**/*.html", "./**/*.svelte"],
        safelist: ["pre", "code"]
      })
    ]
  }
};
var vite_config_default = defineConfig(({ command, mode, ssrBuild }) => {
  if (command === "build") {
    return {
      plugins,
      server,
      css,
      base: "./"
    };
  } else {
    return {
      plugins,
      server
    };
  }
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCIvaG9tZS9wb2xhcmlzL21lbGFtcHVzL2FyY2hpdi9ydW1tYWdpbmdfcm91bmQvbm9kZS5qcy9zbmVsbHlcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIi9ob21lL3BvbGFyaXMvbWVsYW1wdXMvYXJjaGl2L3J1bW1hZ2luZ19yb3VuZC9ub2RlLmpzL3NuZWxseS92aXRlLmNvbmZpZy50c1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vaG9tZS9wb2xhcmlzL21lbGFtcHVzL2FyY2hpdi9ydW1tYWdpbmdfcm91bmQvbm9kZS5qcy9zbmVsbHkvdml0ZS5jb25maWcudHNcIjtpbXBvcnQgeyBzdmVsdGVraXQgfSBmcm9tICdAc3ZlbHRlanMva2l0L3ZpdGUnO1xuaW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSAndml0ZSdcbmltcG9ydCBkc3YgZnJvbSAnQHJvbGx1cC9wbHVnaW4tZHN2J1xuXG5pbXBvcnQgcHVyZ2Vjc3MgZnJvbSAnQGZ1bGxodW1hbi9wb3N0Y3NzLXB1cmdlY3NzJ1xuXG5jb25zdCBwbHVnaW5zID0gW3N2ZWx0ZWtpdCgpLCBkc3YoKV07XG5jb25zdCBzZXJ2ZXIgPSB7XG4gIHBvcnQ6IDUwMDVcbn07XG5jb25zdCBjc3MgPSB7XG4gIHBvc3Rjc3M6IHtcbiAgICBwbHVnaW5zOiBbXG4gICAgICBwdXJnZWNzcyh7XG4gICAgICAgIGNvbnRlbnQ6IFsnLi8qKi8qLmh0bWwnLCAnLi8qKi8qLnN2ZWx0ZSddLFxuICAgICAgICBzYWZlbGlzdDogWydwcmUnLCAnY29kZSddXG4gICAgICB9KVxuICAgIF1cbiAgfVxufTtcblxuLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9cbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbmZpZygoeyBjb21tYW5kLCBtb2RlLCBzc3JCdWlsZCB9KSA9PiB7XG4gIC8vIE9ubHkgcnVuIFB1cmdlQ1NTIGluIHByb2R1Y3Rpb24gYnVpbGRzXG4gIGlmIChjb21tYW5kID09PSAnYnVpbGQnKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHBsdWdpbnMsXG4gICAgICBzZXJ2ZXIsXG4gICAgICBjc3MsXG4gICAgICBiYXNlOiAnLi8nXG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHJldHVybiB7XG4gICAgICBwbHVnaW5zLFxuICAgICAgc2VydmVyXG4gICAgfVxuICB9XG59KVxuIl0sCiAgIm1hcHBpbmdzIjogIjtBQUFzVyxTQUFTLGlCQUFpQjtBQUNoWSxTQUFTLG9CQUFvQjtBQUM3QixPQUFPLFNBQVM7QUFFaEIsT0FBTyxjQUFjO0FBRXJCLElBQU0sVUFBVSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7QUFDbkMsSUFBTSxTQUFTO0FBQUEsRUFDYixNQUFNO0FBQ1I7QUFDQSxJQUFNLE1BQU07QUFBQSxFQUNWLFNBQVM7QUFBQSxJQUNQLFNBQVM7QUFBQSxNQUNQLFNBQVM7QUFBQSxRQUNQLFNBQVMsQ0FBQyxlQUFlLGVBQWU7QUFBQSxRQUN4QyxVQUFVLENBQUMsT0FBTyxNQUFNO0FBQUEsTUFDMUIsQ0FBQztBQUFBLElBQ0g7QUFBQSxFQUNGO0FBQ0Y7QUFHQSxJQUFPLHNCQUFRLGFBQWEsQ0FBQyxFQUFFLFNBQVMsTUFBTSxTQUFTLE1BQU07QUFFM0QsTUFBSSxZQUFZLFNBQVM7QUFDdkIsV0FBTztBQUFBLE1BQ0w7QUFBQSxNQUNBO0FBQUEsTUFDQTtBQUFBLE1BQ0EsTUFBTTtBQUFBLElBQ1I7QUFBQSxFQUNGLE9BQU87QUFDTCxXQUFPO0FBQUEsTUFDTDtBQUFBLE1BQ0E7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
