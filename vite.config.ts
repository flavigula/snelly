import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite'
import dsv from '@rollup/plugin-dsv'

import purgecss from '@fullhuman/postcss-purgecss'

const plugins = [sveltekit(), dsv()];
const server = {
  port: 5005
};
const css = {
  preprocessorOptions: {
    scss: {
    }
  },
  postcss: {
    plugins: [
      purgecss({
        content: ['./**/*.html', './**/*.svelte'],
        safelist: ['pre', 'code']
      })
    ]
  }
};

// https://vitejs.dev/config/
export default defineConfig(({ command, mode, ssrBuild }) => {
  // Only run PurgeCSS in production builds
  if (command === 'build') {
    return {
      plugins,
      server,
      css,
      base: './'
    }
  } else {
    return {
      plugins,
      server
    }
  }
})
