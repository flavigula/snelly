### Install Zep

https://docs.getzep.com/deployment/quickstart/#starting-a-zep-server-locally-is-simple

If it's impossible to get running with docker locally (ie, you are using MACOS), you can start the app with `VITA_ZEP_THURK=true` and it will use the staging Zep server on thurk.org

### Snelly

`git clone https://codeberg.org/flavigula/snelly`

I'm using Node 18.16.0 and Postgres 15.6. Create a `vdna` user in Postgres with a password `thurk`. Create a database `snelly` with `vdna` as its owner. Sorry about the `vdna` username. It's _legacy_. 

Depending on how and where you have Node installed (I recommend `asdf` or one of its clones), you may have to use `sudo` for the following.

`npm install -g db-migrate db-migrate-pg`

`npm install`

`db-migrate --config ./config/database.json up`


`export CHATGPT_API_KEY=92338hsracehualoer8yfa8974f` (replace with actual API key -
 and you may want to place this into an init file (.zshrc, .bashrc, etc))

`VITE_LOCAL=true VITE_CHATGPT_API_KEY=$CHATGPT_API_KEY npm run dev`

Replace `VITE_LOCAL=true` with `VITE_ZEP_THURK` if Zep is not running locally.

The server should start on port 5005.

