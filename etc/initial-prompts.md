# Prompt 1

## Name
Ask Me Anything

## Text
I want you to ask you something. You goal is to help me craft the best possible prompt for you to provide me with the best possible question. You will follow the following process. 1) Your first response will be to ask me what it is I would like to ask. I will provide my answer, but we will need to improve it through continual iterations by going through the next steps. 2) You will ask me 2 questions that will help you give me the best answer. At the end of the questions, you should include text: "I will be asking you questions till you are happy, and then provide you with the best answer or write 'done' to instruct me to generate my answer." I will answer these questions. 3) We will continue this iterative process with me providing additional information to you and you updating the prompt until it is complete.

## Form
Nic

# Prompt 2

## Name
Help Me Write a Blog Article

## Text
1. What should your blog article be about? $$1
2. How many words should your article be? $$2
3. Should there be anything specific mentioned? $$3
4. What is the target audience? $$4
5. What keywords should be included in the article? $$5                                                     
You are a marketing expert. Take my answers to these questions into consideration and write me the best possible blog article you can starting with a title and then the article body making sure not to exceed the limit set in answer #2. If #2 is left blank, restrict your article to 200 words. Do not provide any comments or explanation, only the title and the article text.

## Form
1. What should your blog article be about? $$1
2. How many words should your article be? $$2
3. Should there be anything specific mentioned? $$3
4. What is the target audience? $$4
5. What keywords should be included in the article? $$5                                                     

# Prompt 3

## Name
Generate Blog Post Ideas

## Text
1. What is the target audience for your blog? $$1
2. What are the current trending topics in your niche? $$2
3. What are some common questions or challenges your audience faces? $$3
4. Are there any recent developments or news in your industry? $$4
5. What are some personal experiences or stories you can share? $$5
Based on the answers provided by the user, generate a list of 5 blog post ideas tailored to their target audience and niche. Consider incorporating trending topics, addressing audience questions or challenges, discussing recent developments or news, and leveraging personal experiences or stories.

## Form
1. What is the target audience for your blog? $$1
2. What are the current trending topics in your niche? $$2
3. What are some common questions or challenges your audience faces? $$3
4. Are there any recent developments or news in your industry? $$4
5. What are some personal experiences or stories you can share? $$5

# Prompt 4

## Name
Editing and Proofreading Content

## Text
1. What is the purpose or goal of the content? $$1
2. Who is the intended audience? $$2
3. Are there any specific style or formatting guidelines to follow? $$3
4. Have you reviewed the content for grammar, spelling, and punctuation errors? $$4
5. Does the content flow smoothly and have a logical structure? $$5
Based on the user's responses, provide a comprehensive editing and proofreading checklist that includes reviewing grammar, spelling, and punctuation, ensuring adherence to style guidelines, and improving the overall flow and structure of the content. Offer specific suggestions for improvement where applicable.

## Form
1. What is the purpose or goal of the content? $$1
2. Who is the intended audience? $$2
3. Are there any specific style or formatting guidelines to follow? $$3
4. Have you reviewed the content for grammar, spelling, and punctuation errors? $$4
5. Does the content flow smoothly and have a logical structure? $$5

# Prompt 5

## Name
Outline an Essay Structure

## Text
1. What is the main argument or thesis statement of the essay? $$1
2. What are the key points or supporting evidence for the argument? $$2
3. Are there any counterarguments or opposing viewpoints to address? $$3
4. How should the essay be structured (introduction, body paragraphs, conclusion)? $$4
5. Are there any specific formatting or citation guidelines to follow? $$5
Based on the user's responses, create an outline for the essay that includes the main argument or thesis statement, key points or supporting evidence, addressing counterarguments, and a suggested structure with introduction, body paragraphs, and conclusion. Consider any specific formatting or citation guidelines as provided.

## Form
1. What is the main argument or thesis statement of the essay? $$1
2. What are the key points or supporting evidence for the argument? $$2
3. Are there any counterarguments or opposing viewpoints to address? $$3
4. How should the essay be structured (introduction, body paragraphs, conclusion)? $$4
5. Are there any specific formatting or citation guidelines to follow? $$5

# Prompt 6

## Name
Write Engaging Captions

## Text
1. What is the purpose or goal of the caption? $$1
2. Who is the target audience for the caption? $$2
3. What emotions or reactions do you want to evoke in the audience? $$3
4. Are there any specific keywords or phrases that should be incorporated? $$4
5. Can you provide any relevant context or storytelling elements? $$5
Based on the user's responses, generate an engaging caption that effectively communicates the intended purpose or goal, targets the specific audience, evokes desired emotions or reactions, incorporates relevant keywords or phrases, and potentially includes storytelling elements or relevant context.

## Form
1. What is the purpose or goal of the caption? $$1
2. Who is the target audience for the caption? $$2
3. What emotions or reactions do you want to evoke in the audience? $$3
4. Are there any specific keywords or phrases that should be incorporated? $$4
5. Can you provide any relevant context or storytelling elements? $$5

# Prompt 7

## Name
Language Practise

## Text
1. Which language would you like to practice? $$1
2. What is your current language proficiency level and learning goals? $$2
3. Do you prefer structured exercises or more interactive and immersive activities? $$3
Can you act as a conversation teacher to practice learning the chosen language at the specified level. Make corrections and recommendations so the user can improve their command of the language

## Form
1. Which language would you like to practice? $$1
2. What is your current language proficiency level and learning goals? $$2
3. Do you prefer structured exercises or more interactive and immersive activities? $$3

