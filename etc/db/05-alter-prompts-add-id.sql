ALTER TABLE prompts
  ALTER COLUMN autosubmit
  SET DEFAULT false;

ALTER TABLE prompts
  ADD COLUMN id SERIAL;
