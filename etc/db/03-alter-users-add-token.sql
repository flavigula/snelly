ALTER TABLE users
  ADD COLUMN token varchar(255),
  ADD COLUMN expiry bigint;
