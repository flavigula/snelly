CREATE TABLE prompts (
    prompt          text,
    category        varchar(255),
    skupina         varchar(255), -- group
    vycet           integer, -- order
    form            text,
    use_count       integer,
    name            varchar(255) PRIMARY KEY
);
