1. Hi Bob this is my first questions. What are you thinking about?

2. How can I fire up the landing page in dev env? Should I fiddle around with the routes?
   it may be prudent to create a new route whilst you are working on the landing page.
   /src/routes/landing, for example. We can even redirect to there at first.
   Remember, you need a +layout.svelte & a +page.svelte. Well, you should have both...

3. What does that bit written in Rust in src-tauri do? Looks interesting!
   That stuff was from something that has been abandoned now. I should move it somewhere else. It was a sort of stand alone server.

Question/suggestion
4. From the organisation point of view. Why is landing page in +layout.svelte and not +page.svelte? Just interested not trash talking! :-)
   +layout.svelte is something that shouldn't change (too much) when the user navigates around the site. You can override +landing.svelte in deeper directories, of course, but the point is to have a constant "exterior" design. The +page.svelte on each directory level fills in the <slot> in +landing.svelte
