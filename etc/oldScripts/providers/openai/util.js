import { apiKeyStorage } from '../../store/Store.svelte'
import { get } from 'svelte/store'
import { getApiBase, getEndpointModels } from '../../apiUtil.js'

let _resetSupportedModelsTimer

export const set = (opt) => {
  availableModels = undefined
  apiKeyStorage.set(opt.apiKey || '')
}

const getSupportedModels = async () => {
  if (availableModels) return availableModels
  const openAiKey = get(apiKeyStorage)
  if (!openAiKey) return {}
  try {
        const result = (await (
          await fetch(getApiBase() + getEndpointModels(), {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${openAiKey}`,
              'Content-Type': 'application/json'
            }
          })
        ).json());
        availableModels = result.data.reduce((a, v) => {
          a[v.id] = v
          return a
        }, {})
        return availableModels
  } catch (e) {
        availableModels = {}
        clearTimeout(_resetSupportedModelsTimer)
        _resetSupportedModelsTimer = setTimeout(() => { availableModels = undefined }, 1000)
        return availableModels
  }
}

export const checkModel = async (modelDetail) => {
  const supportedModels = await getSupportedModels()
  if (modelDetail.type === 'chat' || modelDetail.type === 'instruct') {
        modelDetail.enabled = !!supportedModels[modelDetail.modelQuery || '']
  } else {
        // image request.  If we have any models, allow image endpoint
        modelDetail.enabled = !!Object.keys(supportedModels).length
  }
}

