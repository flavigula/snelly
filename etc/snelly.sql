--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: prompts; Type: TABLE; Schema: public; Owner: vdna
--

CREATE TABLE public.prompts (
    prompt text,
    category character varying(255),
    skupina character varying(255),
    vycet integer,
    form text,
    use_count integer,
    name character varying(255) NOT NULL,
    autosubmit boolean DEFAULT false,
    id integer NOT NULL
);


ALTER TABLE public.prompts OWNER TO vdna;

--
-- Name: prompts_id_seq; Type: SEQUENCE; Schema: public; Owner: vdna
--

CREATE SEQUENCE public.prompts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prompts_id_seq OWNER TO vdna;

--
-- Name: prompts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vdna
--

ALTER SEQUENCE public.prompts_id_seq OWNED BY public.prompts.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: vdna
--

CREATE TABLE public.users (
    username character varying(255) NOT NULL,
    password character varying(255),
    name character varying(255),
    token character varying(255),
    expiry bigint
);


ALTER TABLE public.users OWNER TO vdna;

--
-- Name: prompts id; Type: DEFAULT; Schema: public; Owner: vdna
--

ALTER TABLE ONLY public.prompts ALTER COLUMN id SET DEFAULT nextval('public.prompts_id_seq'::regclass);


--
-- Data for Name: prompts; Type: TABLE DATA; Schema: public; Owner: vdna
--

COPY public.prompts (prompt, category, skupina, vycet, form, use_count, name, autosubmit, id) FROM stdin;
I want you to ask you something. You goal is to help me craft the best possible prompt for you to provide me with the best possible question. You will follow the following process. 1) Your first response will be to ask me what it is I would like to ask. I will provide my answer, but we will need to improve it through continual iterations by going through the next steps. 2) You will ask me 2 questions that will help you give me the best answer. At the end of the questions, you should include text: "I will be asking you questions till you are happy, and then provide you with the best answer or write 'done' to instruct me to generate my answer." I will answer these questions. 3) We will continue this iterative process with me providing additional information to you and you updating the prompt until it is complete.	\N	\N	\N	\N	\N	Ask Me Anything	f	1
1. What should your blog article be about? $$1\n2. How many words should your article be? $$2\n3. Should there be anything specific mentioned? $$3 \n4. What is the target audience? $$4\n5. What keywords should be included in the article? $$5\n\nYou are a marketing expert. Take my answers to these questions into consideration and write me the best possible blog article you can starting with a title and then the article body making sure not to exceed the limit set in answer #2. If #2 is left blank, restrict your article to 200 words. Do not provide any comments or explanation, only the title and the article text.	\N	\N	\N	1. What should your blog article be about? $$1\n2. How many words should your article be? $$2\n3. Should there be anything specific mentioned? $$3 \n4. What is the target audience? $$4\n5. What keywords should be included in the article? $$5	\N	Help Me Write a Blog Article	f	2
1. What is the target audience for your blog? $$1\n2. What are the current trending topics in your niche? $$2\n3. What are some common questions or challenges your audience faces? $$3\n4. Are there any recent developments or news in your industry? $$4\n5. What are some personal experiences or stories you can share? $$5\n\nBased on the answers provided by the user, generate a list of 5 blog post ideas tailored to their target audience and niche. Consider incorporating trending topics, addressing audience questions or challenges, discussing recent developments or news, and leveraging personal experiences or stories.	\N	\N	\N	1. What is the target audience for your blog? $$1\n2. What are the current trending topics in your niche? $$2\n3. What are some common questions or challenges your audience faces? $$3\n4. Are there any recent developments or news in your industry? $$4\n4. What are some personal experiences or stories you can share? $$5	\N	Generate Blog Post Ideas	f	3
1. What is the purpose or goal of the content? $$1\n2. Who is the intended audience?  $$2\n3. Are there any specific style or formatting guidelines to follow? $$3\n4. Have you reviewed the content for grammar, spelling, and punctuation errors? $$4\n5. Does the content flow smoothly and have a logical structure? $$5\n\nBased on the user's responses, provide a comprehensive editing and proofreading checklist that includes reviewing grammar, spelling, and punctuation, ensuring adherence to style guidelines, and improving the overall flow and structure of the content. Offer specific suggestions for improvement where applicable.	\N	\N	\N	1. What is the purpose or goal of the content? $$1\n2. Who is the intended audience?  $$2\n3. Are there any specific style or formatting guidelines to follow? $$3\n4. Have you reviewed the content for grammar, spelling, and punctuation errors? $$4\n5. Does the content flow smoothly and have a logical structure? $$5	\N	Editing and proofreading content	f	4
1. What is the main argument or thesis statement of the essay? $$1\n2. What are the key points or supporting evidence for the argument? $$2\n3. Are there any counterarguments or opposing viewpoints to address? $$3\n4. How should the essay be structured (introduction, body paragraphs, conclusion)? $$4\n5. Are there any specific formatting or citation guidelines to follow? $$5\n\nBased on the user's responses, create an outline for the essay that includes the main argument or thesis statement, key points or supporting evidence, addressing counterarguments, and a suggested structure with introduction, body paragraphs, and conclusion. Consider any specific formatting or citation guidelines as provided.	\N	\N	\N	1. What is the main argument or thesis statement of the essay? $$1\n2. What are the key points or supporting evidence for the argument? $$2\n3. Are there any counterarguments or opposing viewpoints to address? $$3\n4. How should the essay be structured (introduction, body paragraphs, conclusion)? $$4\n5. Are there any specific formatting or citation guidelines to follow? $$5	\N	Outline an Essay Structure	f	5
1. What is the purpose or goal of the caption? $$1\n2. Who is the target audience for the caption? $$2\n3. What emotions or reactions do you want to evoke in the audience? $$3\n4. Are there any specific keywords or phrases that should be incorporated? $$4\n5. Can you provide any relevant context or storytelling elements? $$5\n\nBased on the user's responses, generate an engaging caption that effectively communicates the intended purpose or goal, targets the specific audience, evokes desired emotions or reactions, incorporates relevant keywords or phrases, and potentially includes storytelling elements or relevant context.	\N	\N	\N	1. What is the purpose or goal of the caption? $$1\n2. Who is the target audience for the caption? $$2\n3. What emotions or reactions do you want to evoke in the audience? $$3\n4. Are there any specific keywords or phrases that should be incorporated? $$4\n5. Can you provide any relevant context or storytelling elements? $$5	\N	Write Engaging Captions	f	6
1. Which language would you like to practice? $$1\n2. What is your current language proficiency level and learning goals? $$2\n3. Do you prefer structured exercises or more interactive and immersive activities? $$3\n\nCan you act as a conversation teacher to practice learning the chosen language at the specified level. Make corrections and recommendations so the user can improve their command of the language.\n	\N	\N	\N	1. Which language would you like to practice? $$1\n2. What is your current language proficiency level and learning goals? $$2\n3. Do you prefer structured exercises or more interactive and immersive activities? $$3	\N	Language Practice	f	7
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: vdna
--

COPY public.users (username, password, name, token, expiry) FROM stdin;
\.


--
-- Name: prompts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vdna
--

SELECT pg_catalog.setval('public.prompts_id_seq', 7, true);


--
-- Name: prompts prompts_pkey; Type: CONSTRAINT; Schema: public; Owner: vdna
--

ALTER TABLE ONLY public.prompts
    ADD CONSTRAINT prompts_pkey PRIMARY KEY (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: vdna
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);


--
-- PostgreSQL database dump complete
--

