--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: prompts; Type: TABLE; Schema: public; Owner: vdna
--

CREATE TABLE public.prompts (
    id integer NOT NULL,
    prompt_category_ids integer[],
    title character varying,
    question_ids integer[],
    created_at bigint,
    updated_at bigint,
    system_template text,
    assistant_template text
);


ALTER TABLE public.prompts OWNER TO vdna;

--
-- Name: prompts_id_seq; Type: SEQUENCE; Schema: public; Owner: vdna
--

CREATE SEQUENCE public.prompts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prompts_id_seq OWNER TO vdna;

--
-- Name: prompts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vdna
--

ALTER SEQUENCE public.prompts_id_seq OWNED BY public.prompts.id;


--
-- Name: prompts id; Type: DEFAULT; Schema: public; Owner: vdna
--

ALTER TABLE ONLY public.prompts ALTER COLUMN id SET DEFAULT nextval('public.prompts_id_seq'::regclass);


--
-- Data for Name: prompts; Type: TABLE DATA; Schema: public; Owner: vdna
--

COPY public.prompts (id, prompt_category_ids, title, question_ids, created_at, updated_at, system_template, assistant_template) FROM stdin;
2	{2}	Help Me Write a Blog Article	{1,2,3,4,5}	1696403574285	1696403574285	You are a marketing expert. Take the answers to the questions into consideration and write me the best possible blog article you can starting with a title and then the article body making sure not to exceed the word limit. If there is no word limit, restrict the length of the article to 500 words. Do not provide any comments or explanation, only the title and the article text.	1. What should your blog article be about$1 {about} 2. How many words should your article be$2 {word-count} 3. Should there be anything specific mentioned$3 {specifics} 4. What is the target audience$4 {audience} 5. What keywords should be included in the article$5 {keywords}
\.


--
-- Name: prompts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vdna
--

SELECT pg_catalog.setval('public.prompts_id_seq', 2, true);


--
-- Name: prompts prompts_pkey; Type: CONSTRAINT; Schema: public; Owner: vdna
--

ALTER TABLE ONLY public.prompts
    ADD CONSTRAINT prompts_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

